package com.shortcircuit.splatoon;

import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.ArenaManager;
import com.shortcircuit.splatoon.listeners.ArenaListener;
import com.shortcircuit.splatoon.listeners.ServerListener;
import com.shortcircuit.splatoon.listeners.SplatListener;
import com.shortcircuit.splatoon.listeners.WallClimb;
import com.shortcircuit.splatoon.player.*;
import com.shortcircuit.splatoon.util.AnnouncementHandler;
import com.shortcircuit.utils.bukkit.CombinationPluginUpdater;
import com.shortcircuit.utils.bukkit.command.CommandRegister;
import com.shortcircuit.utils.json.JsonConfig;
import com.shortcircuit.utils.json.JsonUtils;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Splatoon extends JavaPlugin {
	private static File arena_file;
	private static Splatoon instance;
	private final ArenaManager arena_manager;
	private final JsonConfig config;
	private final JsonConfig lang_config;
	private final Metrics metrics;

	public Splatoon() {
		instance = this;
		CommandRegister.registerAll(this, "com.shortcircuit.splatoon.commands");
		getDataFolder().mkdirs();
		copyLang();
		config = new JsonConfig(this, new File(getDataFolder() + "/config.json"), "config.json");
		lang_config = new JsonConfig(this, new File(getDataFolder() + "/lang/" + config.getNode("locale", String.class, "en_US") + ".lang"), "lang/en_US.lang");
		arena_file = new File(getDataFolder() + "/arenas.json");
		arena_manager = loadArenaManager();
		config.saveDefaultConfig();
		config.loadConfig();
		Metrics temp_metrics = null;
		try {
			temp_metrics = new Metrics(this);
		}
		catch (IOException e) {
			AnnouncementHandler.announce("metrics.start.failure", "splatoon.metrics", e.getLocalizedMessage());
		}
		metrics = temp_metrics;
	}

	public void onEnable() {
		SquidClass.getRegisteredClasses();
		getLogger().info("Registering listeners");
		getServer().getPluginManager().registerEvents(new ArenaListener(), this);
		getServer().getPluginManager().registerEvents(new SplatListener(), this);
		getServer().getPluginManager().registerEvents(new WallClimb(), this);
		getServer().getPluginManager().registerEvents(new ServerListener(), this);
		getLogger().info("Registered");
		getLogger().info("Registering default classes");
		new Roller().register();
		new Carrier().register();
		new Charger().register();
		new Shooter().register();
		getLogger().info("Registered");
		if (config.getNode("check_updates", boolean.class, true)) {
			new CombinationPluginUpdater(this, 11069, "splatoon-ultimate", config.getNode("auto_update", boolean.class, false));
		}
		if (metricsCreated()) {
			metrics.start();
			AnnouncementHandler.announce("metrics.start.success", "splatoon.metrics");
		}
	}

	public void onDisable() {
		for (Arena arena : arena_manager.getArenas()) {
			if (arena.getMatchHandler().hasMatch()) {
				if (arena.getMatchHandler().isMatchStarted()) {
					arena.getMatchHandler().getCurrentMatch().getTimer().stop();
				}
				else {
					arena.getMatchHandler().getCurrentMatch().stopMatch();
				}
			}
		}
		saveArenaManager(arena_manager);
		config.saveConfig();
		lang_config.saveConfig();
	}

	public static Splatoon getInstance() {
		return instance;
	}

	private ArenaManager loadArenaManager() {
		try {
			if (!arena_file.exists()) {
				saveArenaManager(new ArenaManager());
			}
			StringBuilder builder = new StringBuilder();
			Scanner scanner = new Scanner(arena_file);
			while (scanner.hasNextLine()) {
				builder.append(scanner.nextLine());
			}
			ArenaManager temp_manager = JsonUtils.fromJson(builder.toString(), ArenaManager.class);
			if (temp_manager == null) {
				temp_manager = new ArenaManager();
			}
			return temp_manager;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void saveArenaManager(ArenaManager arena_manager) {
		arena_manager.organizeArenas();
		try {
			arena_file.createNewFile();
			FileWriter writer = new FileWriter(arena_file);
			writer.write(JsonUtils.toJsonString(arena_manager));
			writer.flush();
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArenaManager getArenaManager() {
		return arena_manager;
	}

	public JsonConfig getJsonConfig() {
		return config;
	}

	@Override
	public File getFile() {
		return super.getFile();
	}

	public Metrics getMetrics() {
		return metrics;
	}

	public JsonConfig getLangConfig() {
		return lang_config;
	}

	public boolean metricsCreated() {
		return metrics != null;
	}

	private void copyLang() {
		try {
			File top = new File(getDataFolder() + "/lang/");
			top.mkdir();
			final String[] locales = new String[]{
					"en_US",
			};
			for (String locale : locales) {
				File lang_file = new File(getDataFolder() + "/lang/" + locale + ".lang");
				if (lang_file.exists()) {
					continue;
				}
				Files.copy(Splatoon.class.getResourceAsStream("/lang/" + locale + ".lang"), lang_file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
