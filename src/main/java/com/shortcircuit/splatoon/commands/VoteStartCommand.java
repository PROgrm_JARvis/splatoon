package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.PlayerVoteEvent;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class VoteStartCommand extends BaseCommand<Player> {
	public VoteStartCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String getCommandName() {
		return "votestart";
	}

	@Override
	public String[] getCommandAliases() {
		return null;
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.vote";
	}

	@Override
	public String[] exec(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(sender.getUniqueId());
		if (inkling == null) {
			return new String[]{ChatColor.RED + "You are not waiting for a match"};
		}
		if (inkling.hasVoted()) {
			return new String[]{ChatColor.RED + "You have already submitted your vote"};
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getID());
		if (arena.getMatchHandler().isMatchStarted()) {
			return new String[]{ChatColor.RED + "Match has already started"};
		}
		Match match = arena.getMatchHandler().getCurrentMatch();
		PlayerVoteEvent event = new PlayerVoteEvent(inkling, match);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (!event.isCancelled()) {
			match.submitStartVote(inkling);
			return new String[]{ChatColor.GOLD + "You have submitted your vote"};
		}
		return new String[]{};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command>"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Submits your vote to start the match"
		};
	}
}
