package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.MatchHandler;
import com.shortcircuit.splatoon.game.event.MatchEndEvent;
import com.shortcircuit.splatoon.game.event.MatchStartEvent;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.NoPermissionException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class MatchCommand extends BaseCommand<CommandSender> {
	public MatchCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String getCommandName() {
		return "splatoonmatch";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatmatch", "smatch"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.match";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> <create|start|stop|cancel> <arena>",
				"Create: Creates a new match",
				"Start: Forces the match to start, regardless of player count",
				"Stop: Forces the match to stop",
				"Cancel: Cancels a queued match"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Create or manage matches"
		};
	}

	@Override
	public String[] exec(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
		if (arena == null) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		MatchHandler match_handler = arena.getMatchHandler();
		switch (args.get(0).toLowerCase()) {
			case "create":
				if (!PermissionUtils.hasPermission(sender, "splatoon.match.create")) {
					throw new NoPermissionException();
				}
				if (match_handler.isMatchStarted()) {
					return new String[]{ChatColor.RED + "Match is already in progress"};
				}
				match_handler.newMatch();
				return new String[]{};
			case "start":
				if (!PermissionUtils.hasPermission(sender, "splatoon.match.start")) {
					throw new NoPermissionException();
				}
				if (!match_handler.hasMatch()) {
					return new String[]{ChatColor.RED + "The arena does not have a match queued"};
				}
				if (match_handler.isMatchStarted()) {
					return new String[]{ChatColor.RED + "Match is already in progress"};
				}
				match_handler.getCurrentMatch().doStartMatch();
				Bukkit.getServer().getPluginManager().callEvent(new MatchStartEvent(match_handler.getCurrentMatch(), MatchStartEvent.Cause.COMMAND));
				return new String[]{};
			case "cancel":
				if (!PermissionUtils.hasPermission(sender, "splatoon.match.cancel")) {
					throw new NoPermissionException();
				}
				if (!match_handler.hasMatch()) {
					return new String[]{ChatColor.RED + "No match queued"};
				}
				if (match_handler.isMatchStarted()) {
					return new String[]{ChatColor.RED + "Match is already in progress"};
				}
				match_handler.getCurrentMatch().stopMatch();
				Bukkit.getServer().getPluginManager().callEvent(new MatchEndEvent(match_handler.getCurrentMatch(), MatchEndEvent.Cause.COMMAND));
				return new String[]{};
			case "stop":
				if (!PermissionUtils.hasPermission(sender, "splatoon.match.stop")) {
					throw new NoPermissionException();
				}
				if (!match_handler.hasMatch()) {
					return new String[]{ChatColor.RED + "The arena does not have a match queued"};
				}
				if (!match_handler.isMatchStarted()) {
					return new String[]{ChatColor.RED + "No match in progress"};
				}
				match_handler.getCurrentMatch().stopMatch();
				Bukkit.getServer().getPluginManager().callEvent(new MatchEndEvent(match_handler.getCurrentMatch(), MatchEndEvent.Cause.COMMAND));
				return new String[]{};
			default:
				throw new InvalidArgumentException("Unknown operation: " + args.get(0));
		}
	}
}
