package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class JoinGameCommand extends BaseCommand<Player> {
	public JoinGameCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String getCommandName() {
		return "joinsplatoon";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"joinsplat", "quitsplatoon", "quitsplat", "leavesplatoon", "leavesplat"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.match.join";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/joinsplatoon <arena>",
				"Automatically places you on the team with the fewest players",
				"/joinsplatoon <arena> <class>",
				"Join a match",
				"/joinsplatoon <arena> <class> [team]",
				"Join a match, placing you on the given team",
				"/quitsplatoon",
				"Leave a match"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Join or leave a match"
		};
	}

	@Override
	public String[] exec(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		switch (command.toLowerCase()) {
			case "joinsplatoon":
			case "joinsplat":
				if (args.size() < 1) {
					throw new TooFewArgumentsException();
				}
				if (Splatoon.getInstance().getArenaManager().getArenaContainingInkling(sender.getUniqueId()) != null) {
					return new String[]{ChatColor.RED + "You are already in a game"};
				}
				int arena_id;
				try {
					arena_id = Integer.parseInt(args.get(0));
				}
				catch (NumberFormatException e) {
					throw new InvalidArgumentException("Second argument must be a positive integer");
				}
				Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
				if (arena == null) {
					return new String[]{ChatColor.RED + "No arena with id " + arena_id};
				}
				if (!arena.getMatchHandler().hasMatch()) {
					return new String[]{ChatColor.RED + "The arena does not have a match queued"};
				}
				if (arena.getMatchHandler().isMatchStarted()) {
					return new String[]{ChatColor.RED + "Match is already in progress"};
				}
				Match match = arena.getMatchHandler().getCurrentMatch();
				TeamColor team;
				SquidClass squid_class;
				if (args.size() == 1) {
					team = match.getDeficientTeam();
					squid_class = match.getDeficientClass();
				}
				else {
					squid_class = SquidClass.getSquidClass(args.get(1));
					if (squid_class == null) {
						return new String[]{
								ChatColor.RED + args.get(1) + " is not a valid class",
								ChatColor.RED + "Type \"/splatooninfo classes\" to view a list of classes"
						};
					}
					if (!match.getDeficientClasses().contains(squid_class)) {
						LinkedList<SquidClass> deficient = match.getDeficientClasses();
						StringBuilder builder = new StringBuilder();
						for (SquidClass deficient_class : deficient) {
							builder.append(deficient_class.getClassName()).append(ChatColor.AQUA).append(", ");
						}
						String available = builder.toString();
						available = available.substring(0, available.lastIndexOf(','));
						return new String[]{
								"That class is full right now",
								"Please choose another class, or wait until that class is available",
								"Available classes are: " + available
						};
					}
					if (args.size() == 2) {
						team = match.getDeficientTeam();
					}
					else {
						team = TeamColor.getTeam(args.get(2));
						if (!match.getDeficientTeams().contains(team)) {
							LinkedList<TeamColor> deficient = match.getDeficientTeams();
							StringBuilder builder = new StringBuilder();
							for (TeamColor deficient_color : deficient) {
								builder.append(deficient_color.getTeamName()).append(ChatColor.AQUA).append(", ");
							}
							String available = builder.toString();
							available = available.substring(0, available.lastIndexOf(','));
							return new String[]{
									"That team is full right now",
									"Please choose another team, or wait until that team is available",
									"Available teams are: " + available
							};
						}
					}
				}
				match.addInkling(sender, team, squid_class);
				break;
			case "leavesplatoon":
			case "leavesplat":
			case "quitsplatoon":
			case "quitsplat":
				arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(sender.getUniqueId());
				if (arena == null) {
					return new String[]{ChatColor.RED + "You are not in a game"};
				}
				arena.getMatchHandler().getCurrentMatch().removeInkling(sender.getUniqueId());
				break;
		}
		return new String[]{};
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
		ArrayList<String> suggestions = new ArrayList<>(4);
		switch (alias) {
			case "joinsplatoon":
			case "joinsplat":
				int arena_id;
				try {
					arena_id = Integer.parseInt(args[0]);
				}
				catch (NumberFormatException e) {
					break;
				}
				Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
				if (args.length == 2) {
					if (arena == null || !arena.getMatchHandler().hasMatch()) {
						for (SquidClass squid_class : SquidClass.getRegisteredClasses()) {
							if (squid_class.getClassName().toLowerCase().contains(args[1].toLowerCase())) {
								suggestions.add(ChatColor.stripColor(squid_class.getClassName()));
							}
						}
					}
					else {
						for (SquidClass squid_class : arena.getMatchHandler().getCurrentMatch().getDeficientClasses()) {
							if (squid_class.getClassName().toLowerCase().contains(args[1].toLowerCase())) {
								suggestions.add(ChatColor.stripColor(squid_class.getClassName()));
							}
						}
					}
				}
				else if (args.length == 3) {
					sender.sendMessage(args[2]);
					if (arena == null || !arena.getMatchHandler().hasMatch()) {
						for (TeamColor team : TeamColor.values()) {
							if (team.getTeamName().toLowerCase().contains(args[2].toLowerCase())) {
								suggestions.add(ChatColor.stripColor(team.getTeamName()));
							}
						}
					}
					else {
						for (TeamColor team : arena.getMatchHandler().getCurrentMatch().getDeficientTeams()) {
							if (team.getTeamName().toLowerCase().contains(args[2].toLowerCase())) {
								suggestions.add(ChatColor.stripColor(team.getTeamName()));
							}
						}
					}
				}
		}
		return suggestions;
	}
}
