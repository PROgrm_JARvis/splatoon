package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.util.SchematicUtil;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.PlayerOnlyException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.world.World;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class ArenaCommand extends BaseCommand<CommandSender> {
	public ArenaCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String getCommandName() {
		return "splatoonarena";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatarena", "sarena"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.arena";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> create",
				"Creates a new arena within your WorldEdit selection",
				"/<command> remove <arena>",
				"Deletes an arena",
				"/<command> save <arena>",
				"Creates a new arena backup",
				"/<command> load <arena>",
				"Loads an arena from its backup",
				"/<command> disable <arena|all>",
				"Disables one or all arenas",
				"/<command> enable <arena|all>",
				"Enables one or all arenas"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Create or manage arenas"
		};
	}

	@Override
	public String[] exec(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 1) {
			throw new TooFewArgumentsException();
		}
		switch (args.get(0).toLowerCase()) {
			case "new":
			case "create":
				if (!(sender instanceof Player)) {
					throw new PlayerOnlyException();
				}
				return createArena((Player) sender);
			case "remove":
			case "delete":
				return removeArena(args);
			case "save":
				return saveArena(args);
			case "load":
			case "regen":
			case "regenerate":
				return loadArena(args);
			case "enable":
				return enableArena(args);
			case "disable":
				return disableArena(args);
		}
		return new String[]{};
	}

	private String[] enableArena(ConcurrentArrayList<String> args) throws CommandException{
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		if(args.get(1).equalsIgnoreCase("all")){
			for(Arena arena : Splatoon.getInstance().getArenaManager().getArenas()){
				arena.setEnabled(true);
			}
			return new String[]{ChatColor.GOLD + "All arenas enabled"};
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		if (!Splatoon.getInstance().getArenaManager().arenaExists(arena_id)) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Splatoon.getInstance().getArenaManager().getArena(arena_id).setEnabled(true);
		return new String[]{ChatColor.GOLD + "Enabled arena #" + arena_id};
	}

	private String[] disableArena(ConcurrentArrayList<String> args) throws CommandException{
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		if(args.get(1).equalsIgnoreCase("all")){
			for(Arena arena : Splatoon.getInstance().getArenaManager().getArenas()){
				arena.setEnabled(false);
			}
			return new String[]{ChatColor.GOLD + "All arenas disabled"};
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		if (!Splatoon.getInstance().getArenaManager().arenaExists(arena_id)) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Splatoon.getInstance().getArenaManager().getArena(arena_id).setEnabled(false);
		return new String[]{ChatColor.GOLD + "Disabled arena #" + arena_id};
	}

	private String[] createArena(Player player) {
		Splatoon.getInstance().getArenaManager().organizeArenas();
		LocalSession session = WorldEdit.getInstance().getSessionManager().findByName(player.getName());
		if (session == null || !session.isSelectionDefined(BukkitUtil.getLocalWorld(player.getWorld()))) {
			return new String[]{ChatColor.RED + "No selection defined"};
		}
		Region selection;
		World world = session.getSelectionWorld();
		try {
			selection = session.getSelection(session.getSelectionWorld());
		}
		catch (IncompleteRegionException e) {
			return new String[]{e.getMessage()};
		}
		Vector min_pos = selection.getMinimumPoint();
		Vector max_pos = selection.getMaximumPoint();
		Arena arena = new Arena(new Location(Bukkit.getWorld(world.getName()), min_pos.getBlockX(), min_pos.getBlockY(), min_pos.getBlockZ()),
				new Location(Bukkit.getWorld(world.getName()), max_pos.getBlockX(), max_pos.getBlockY(), max_pos.getBlockZ()));
		Splatoon.getInstance().getArenaManager().addArena(arena);
		return new String[]{ChatColor.GOLD + "Created arena #" + arena.getID()};
	}

	private String[] removeArena(ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		if (!Splatoon.getInstance().getArenaManager().arenaExists(arena_id)) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Splatoon.getInstance().getArenaManager().removeArena(arena_id);
		return new String[]{ChatColor.GOLD + "Removed arena #" + arena_id};
	}

	private String[] saveArena(ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		if (!Splatoon.getInstance().getArenaManager().arenaExists(arena_id)) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
		if (SchematicUtil.saveArena(arena)) {
			return new String[]{ChatColor.GOLD + "Saved arena #" + arena_id};
		}
		return new String[]{
				ChatColor.RED + "Could not save arena",
				ChatColor.RED + "Please check the console for errors"};
	}

	private String[] loadArena(ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(1));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("Second argument must be a positive integer");
		}
		if (!Splatoon.getInstance().getArenaManager().arenaExists(arena_id)) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
		if (SchematicUtil.regenArena(arena)) {
			return new String[]{ChatColor.GOLD + "Regenerated arena #" + arena_id};
		}
		return new String[]{
				ChatColor.RED + "Could not regenerate arena",
				ChatColor.RED + "Please check the console for errors"};
	}
}
