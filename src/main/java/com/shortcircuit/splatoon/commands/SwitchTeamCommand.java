package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.event.PlayerChangeClassEvent;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SwitchTeamCommand extends BaseCommand<Player> {
	public SwitchTeamCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String getCommandName() {
		return "switchsplatoon";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"switchsplat", "changesplatoon", "chgsplatoon", "changesplat", "chgsplat"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.switchsplat";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"Syntax: /switchsplat <class>",
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Changes your squid class before a game starts"
		};
	}

	@Override
	public String[] exec(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 1) {
			throw new TooFewArgumentsException();
		}
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(sender.getUniqueId());
		if (inkling == null) {
			return new String[]{ChatColor.RED + "You are not waiting for a match"};
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getID());
		if (arena.getMatchHandler().isMatchStarted()) {
			return new String[]{ChatColor.RED + "Match has already started"};
		}
		SquidClass squid_class = SquidClass.getSquidClass(args.get(0));
		if (squid_class == null) {
			return new String[]{ChatColor.RED + "Unknown squid class:" + args.get(0)};
		}
		PlayerChangeClassEvent event = new PlayerChangeClassEvent(inkling,
				arena.getMatchHandler().getCurrentMatch(), inkling.getSquidClass(), squid_class);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (!event.isCancelled()) {
			inkling.setSquidClass(event.getNewClass());
			return new String[]{ChatColor.GOLD + "You are now a " + event.getNewClass().getClassName()};
		}
		return new String[]{};
	}
}
