package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.statistics.LobbySign;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.bukkit.BukkitUtils;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SplatSignCommand extends BaseCommand<Player> {
	public SplatSignCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PLAYER;
	}

	@Override
	public String getCommandName() {
		return "splatoonsign";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatsign", "ssign"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.setsign";
	}

	@Override
	public String[] exec(Player sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 1) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(0));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("The first argument must be a positive integer");
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
		if (arena == null) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		Block block = sender.getTargetBlock(BukkitUtils.getLegacyTransparentBlocks(), 5);
		if (block == null || !(block.getState() instanceof Sign)) {
			return new String[]{ChatColor.RED + "You are not targeting a sign"};
		}
		if (args.size() == 1) {
			arena.getStatusSign().setSignLocation(block.getLocation());
		}
		else {
			SquidClass squid_class = null;
			TeamColor team = null;
			if (args.size() >= 2) {
				squid_class = SquidClass.getSquidClass(args.get(1));
				if (squid_class == null) {
					team = TeamColor.getTeam(args.get(1));
				}
				if (args.size() >= 3) {
					if (squid_class == null) {
						squid_class = SquidClass.getSquidClass(args.get(2));
					}
					if (team == null) {
						team = TeamColor.getTeam(args.get(2));
					}
				}
			}
			LobbySign sign = null;
			for (LobbySign lobby_sign : arena.getLobbySigns()) {
				if (lobby_sign.getSignLocation().equals(block.getLocation())) {
					sign = lobby_sign;
					break;
				}
			}
			if (sign != null) {
				if (squid_class == null) {
					squid_class = sign.getJoinClass();
				}
				if (team == null) {
					team = sign.getJoinColor();
				}
				sign.setJoinClass(squid_class);
				sign.setJoinColor(team);
			}
			else {
				sign = new LobbySign(block.getLocation(), arena_id, squid_class, team);
				arena.getLobbySigns().add(sign);
			}
		}
		return new String[]{"Marked sign for use by arena #" + arena_id};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> <arena> [class] [team]"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Marks a sign for use by an arena for status displays"
		};
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
		ArrayList<String> suggestions = new ArrayList<>(4);
		if (args.length >= 2) {
			for (SquidClass squid_class : SquidClass.getRegisteredClasses()) {
				if (squid_class.getClassName().toLowerCase().contains(args[1].toLowerCase())) {
					suggestions.add(ChatColor.stripColor(squid_class.getClassName()));
				}
			}
			for (TeamColor color : TeamColor.values()) {
				if (color.getTeamName().toLowerCase().contains(args[1].toLowerCase())) {
					suggestions.add(ChatColor.stripColor(color.getTeamName()));
				}
			}
		}
		return suggestions;
	}
}
