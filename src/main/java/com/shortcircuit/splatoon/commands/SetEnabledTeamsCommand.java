package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SetEnabledTeamsCommand extends BaseCommand<CommandSender> {
	public SetEnabledTeamsCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String getCommandName() {
		return "splatoonteams";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatteams"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.arena.teams";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> <arena> <teams...>"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Sets which teams are enabled in a given arena"
		};
	}

	@Override
	public String[] exec(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 2) {
			throw new TooFewArgumentsException();
		}
		int arena_id;
		try {
			arena_id = Integer.parseInt(args.get(0));
		}
		catch (NumberFormatException e) {
			throw new InvalidArgumentException("First argument must be a positive integer");
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
		if (arena == null) {
			return new String[]{ChatColor.RED + "No arena with id " + arena_id};
		}
		if (arena.getMatchHandler().hasMatch()) {
			return new String[]{ChatColor.RED + "The arena already has a match queued"};
		}
		String[] new_args = new String[args.size() - 1];
		System.arraycopy(args.toArray(new String[0]), 1, new_args, 0, new_args.length);
		ArrayList<TeamColor> enabled_teams = new ArrayList<>(4);
		for (String arg : new_args) {
			if (arg.contains(",")) {
				for (String name : arg.split(",")) {
					TeamColor team = TeamColor.getTeam(name.trim());
					if (team == null) {
						sender.sendMessage(ChatColor.RED + "Invalid team: " + name.trim());
					}
					enabled_teams.add(team);
				}
			}
			else {
				TeamColor team = TeamColor.getTeam(arg);
				if (team == null) {
					sender.sendMessage(ChatColor.RED + "Invalid team: " + arg.trim());
				}
				enabled_teams.add(team);
			}
		}
		if (enabled_teams.size() < 2) {
			return new String[]{ChatColor.RED + "Not enough valid teams were provided"};
		}
		arena.setEnabledTeams(enabled_teams);
		return new String[]{"Enabled teams set to: " + ChatColor.stripColor(Arrays.toString(arena.getEnabledTeams().toArray()))};
	}
}
