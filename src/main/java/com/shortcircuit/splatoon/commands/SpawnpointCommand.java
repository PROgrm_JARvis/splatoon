package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.LocatableCommandSender;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SpawnpointCommand extends BaseCommand<LocatableCommandSender> {
	public SpawnpointCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.LOCATABLE;
	}

	@Override
	public String getCommandName() {
		return "splatoonspawn";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatspawn"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.arena.spawn";
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{
				"/<command> <team>",
				"Sets the team spawnpoint",
				"/<command> exit <arena>",
				"Sets the exit point for the arena"
		};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"Set the spawnpoints of an arena"
		};
	}

	@Override
	public String[] exec(LocatableCommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() < 1) {
			throw new TooFewArgumentsException();
		}
		Arena arena;
		if (args.get(0).equalsIgnoreCase("exit")) {
			if (args.size() < 2) {
				throw new TooFewArgumentsException();
			}
			int arena_id;
			try {
				arena_id = Integer.parseInt(args.get(1));
			}
			catch (NumberFormatException e) {
				throw new InvalidArgumentException("Second argument must be a whole number");
			}
			arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
			if (arena == null) {
				return new String[]{ChatColor.RED + "No arena with id " + arena_id};
			}
			arena.setExitPos(sender.getLocation());
			return new String[]{ChatColor.GOLD + "Set the exit point for the arena"};
		}
		arena = Splatoon.getInstance().getArenaManager().getArenaByLocation(sender.getLocation());
		if (arena == null) {
			return new String[]{ChatColor.RED + "You are not inside an arena"};
		}
		TeamColor team = TeamColor.getTeam(args.get(0));
		if (team == null) {
			throw new InvalidArgumentException("Unknown team: " + args.get(0));
		}
		arena.setTeamSpawn(team, sender.getLocation());
		return new String[]{ChatColor.GOLD + "Set the spawnpoint for team " + team.getTeamName()};
	}
}
