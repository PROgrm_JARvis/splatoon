package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.powerups.Infection;
import com.shortcircuit.splatoon.util.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 12/1/2015
 */
public class Carrier extends SquidClass {
	private static final ItemStack INK_VIAL = new ItemStack(Material.POTION, 1);

	static {
		ItemMeta meta = INK_VIAL.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.carrier.tool", String.class, "&d&oInk vial")));
		LinkedList<String> lore = new LinkedList<>();
		lore.add(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.tool.lore", String.class, "&dSpreads <team> ink")));
		meta.setLore(lore);
		INK_VIAL.setItemMeta(meta);
	}

	public Carrier() {
		super(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.carrier.name", String.class, "&bCarrier")));
	}

	@Override
	public ItemStack cloneGunItem(TeamColor team) {
		return INK_VIAL.clone();
	}

	@Override
	public String getDescription() {
		return Splatoon.getInstance().getLangConfig().getNode("inkling.carrier.desc", String.class,
				"This class plants an infection that slowly paints a large area on any surface");
	}

	@EventHandler
	public void splat(final PlayerInteractEvent event) {
		if (!event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			return;
		}
		Block block = event.getClickedBlock();
		if (block == null) {
			return;
		}
		Player player = event.getPlayer();
		if (event.getItem() == null || !event.getItem().getType().equals(Material.POTION)) {
			return;
		}
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if (inkling == null || inkling.isDead() || inkling.getAmmunition() < 4 ||  !(inkling.getSquidClass() instanceof Carrier)
				|| !Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId()).getMatchHandler().isMatchStarted()) {
			return;
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getPlayer().getUniqueId());
		if (!Utils.isInArena(event.getClickedBlock().getLocation(), arena)) {
			return;
		}
		inkling.removeAmmunition(4);
		new Infection(inkling, block, 1.0, arena, inkling.getTeam());
	}
}
