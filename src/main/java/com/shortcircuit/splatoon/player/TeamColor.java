package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public enum TeamColor {
	RED(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
			.getNode("team.red", String.class, "&cRed")),
			Splatoon.getInstance().getJsonConfig().getNode("team.red.color", byte.class, (byte) 14)),
	YELLOW(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
			.getNode("team.yellow", String.class, "&eYellow")),
			Splatoon.getInstance().getJsonConfig().getNode("team.yellow.color", byte.class, (byte) 4)),
	GREEN(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
			.getNode("team.green", String.class, "&aGreen")),
			Splatoon.getInstance().getJsonConfig().getNode("team.green.color", byte.class, (byte) 5)),
	BLUE(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
			.getNode("team.blue", String.class, "&bBlue")),
			Splatoon.getInstance().getJsonConfig().getNode("team.blue.color", byte.class, (byte) 3));

	private final String team_name;
	private final byte color_data;
	private final byte dye_color;

	TeamColor(String team_name, byte color_data) {
		this.team_name = team_name;
		this.color_data = color_data;
		this.dye_color = DyeColor.getByWoolData(color_data).getDyeData();
	}

	public String getTeamName() {
		return team_name;
	}

	public byte getDyeColor(){
		return dye_color;
	}

	public byte getColorData() {
		return color_data;
	}

	public static TeamColor getTeam(String name) {
		if (name == null) {
			return null;
		}
		name = ChatColor.stripColor(name);
		if (name.equalsIgnoreCase(ChatColor.stripColor(RED.getTeamName()))) {
			return RED;
		}
		if (name.equalsIgnoreCase(ChatColor.stripColor(YELLOW.getTeamName()))) {
			return YELLOW;
		}
		if (name.equalsIgnoreCase(ChatColor.stripColor(GREEN.getTeamName()))) {
			return GREEN;
		}
		if (name.equalsIgnoreCase(ChatColor.stripColor(BLUE.getTeamName()))) {
			return BLUE;
		}
		return null;
	}

	public static TeamColor getTeam(byte color) {
		if (color == RED.color_data) {
			return RED;
		}
		if (color == YELLOW.color_data) {
			return YELLOW;
		}
		if (color == GREEN.color_data) {
			return GREEN;
		}
		if (color == BLUE.color_data) {
			return BLUE;
		}
		return null;
	}

	@Override
	public String toString() {
		return getTeamName();
	}
}