package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 12/1/2015
 */
public class Shooter extends SquidClass {
	private static final ItemStack INK_GUN = new ItemStack(Material.BLAZE_ROD, 1);

	static {
		ItemMeta meta = INK_GUN.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.shooter.tool", String.class, "&d&oInk gun")));
		LinkedList<String> lore = new LinkedList<>();
		lore.add(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.tool.lore", String.class, "&dSpreads <team> ink")));
		meta.setLore(lore);
		INK_GUN.setItemMeta(meta);
	}

	public Shooter() {
		super(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.shooter.name", String.class, "&dShooter")));
	}

	@Override
	public ItemStack cloneGunItem(TeamColor team) {
		return INK_GUN.clone();
	}

	@Override
	public String getDescription() {
		return Splatoon.getInstance().getLangConfig().getNode("inkling.shooter.desc", String.class,
				"This class shoots a constant stream of ink to paint a small area on any surface");
	}

	@EventHandler
	public void shootSplats(final PlayerInteractEvent event) {
		if (!event.hasItem()) {
			return;
		}
		Player player = event.getPlayer();
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if(inkling == null || inkling.isDead() || inkling.getAmmunition() < 2 ||  !(inkling.getSquidClass() instanceof Shooter)
				|| !Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId()).getMatchHandler().isMatchStarted()) {
			return;
		}
		ItemStack item = event.getItem();
		if (!item.getType().equals(Material.BLAZE_ROD)) {
			return;
		}
		inkling.removeAmmunition(2);
		event.getPlayer().launchProjectile(Snowball.class);
	}
}
