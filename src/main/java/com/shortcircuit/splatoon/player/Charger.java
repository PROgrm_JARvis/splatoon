package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.MatchHandler;
import com.shortcircuit.splatoon.game.timers.ChargerTimer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 12/1/2015
 */
public class Charger extends SquidClass {
	private static final ItemStack INK_CHARGER = new ItemStack(Material.BOW, 1);
	private final ArrayList<ChargerTimer> charger_timers = new ArrayList<>();

	static {
		ItemMeta meta = INK_CHARGER.getItemMeta();
		meta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		meta.addEnchant(Enchantment.DURABILITY, 128, true);
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.charger.tool", String.class, "&d&oInk charger")));
		LinkedList<String> lore = new LinkedList<>();
		lore.add(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.tool.lore", String.class, "&dSpreads <team> ink")));
		meta.setLore(lore);
		INK_CHARGER.setItemMeta(meta);
	}

	public Charger() {
		super(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.charger.name", String.class, "&aCharger")));
	}

	@Override
	public ItemStack cloneGunItem(TeamColor team) {
		return INK_CHARGER.clone();
	}

	@Override
	public ItemStack[] cloneAdditionalItems(TeamColor team) {
		return new ItemStack[]{new ItemStack(Material.ARROW, 1)};
	}

	@Override
	public String getDescription() {
		return Splatoon.getInstance().getLangConfig().getNode("inkling.charger.desc", String.class,
				"This class charges its weapon to release a glob of ink, painting a trail as it goes");
	}

	@EventHandler
	public void shootSplat(final ProjectileLaunchEvent event) {
		if (!(event.getEntity().getShooter() instanceof Player) || !event.getEntityType().equals(EntityType.ARROW)) {
			return;
		}
		Player player = (Player) event.getEntity().getShooter();
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if(inkling == null || inkling.isDead() || inkling.getAmmunition() < 16 ||  !(inkling.getSquidClass() instanceof Charger)
				|| !Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId()).getMatchHandler().isMatchStarted()) {
			return;
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId());
		if(arena == null){
			return;
		}
		MatchHandler match_handler = arena.getMatchHandler();
		if (inkling.isDead() || inkling.isDisguised()
				|| !match_handler.isMatchStarted()
				|| !(inkling.getSquidClass() instanceof Charger)) {
			return;
		}
		inkling.removeAmmunition(16);
		charger_timers.add(new ChargerTimer(inkling, match_handler.getCurrentMatch(), (Arrow) event.getEntity(), Splatoon.getInstance().getArenaManager()
				.getArenaContainingInkling(inkling.getID()).getMatchHandler().getCurrentMatch().getStatistics(), inkling.getTeam()));
	}

	@EventHandler
	public synchronized void cancelChargerTimer(final ProjectileHitEvent event) {
		if (!event.getEntityType().equals(EntityType.ARROW)) {
			return;
		}
		Arrow arrow = (Arrow) event.getEntity();
		ChargerTimer to_remove = null;
		for (ChargerTimer timer : charger_timers) {
			if (arrow.getUniqueId().equals(timer.getArrowID())) {
				to_remove = timer;
				break;
			}
		}
		if (to_remove != null) {
			to_remove.cancel();
			charger_timers.remove(to_remove);
		}
	}
}
