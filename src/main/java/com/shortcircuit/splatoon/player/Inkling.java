package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.timers.RespawnTimer;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Inkling {
	private final Player player;
	private final UUID id;
	private final TeamColor team;
	private final long match_id;
	private boolean is_dead = false;
	private final Scoreboard old_scoreboard;
	private final GameMode old_gamemode;
	private final ItemStack[] old_inventory;
	private final ItemStack[] old_armor;
	private final int old_level;
	private final float old_exp;
	private final float old_walkspeed;
	private SquidClass squid_class;
	private boolean disguised = false;
	private boolean voted = false;
	private RespawnTimer respawn_timer;
	private final ItemStack ammunition = new ItemStack(Material.INK_SACK, 64);

	public Inkling(Player player, TeamColor team, SquidClass squid_class, long match_id) {
		this.player = player;
		this.id = player.getUniqueId();
		this.team = team;
		this.match_id = match_id;
		this.old_armor = player.getInventory().getArmorContents().clone();
		this.old_scoreboard = player.getScoreboard();
		this.old_gamemode = player.getGameMode();
		this.old_inventory = player.getInventory().getContents().clone();
		this.old_level = player.getLevel();
		this.old_exp = player.getExp();
		this.old_walkspeed = player.getWalkSpeed();
		player.setHealth(player.getMaxHealth());
		player.setFoodLevel(20);
		player.setLevel(0);
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
		ItemMeta meta = ammunition.getItemMeta();
		meta.setDisplayName(team.getTeamName() + " ink reservoir");
		ammunition.setDurability(team.getDyeColor());
		setSquidClass(squid_class);
	}

	public Player getPlayer() {
		return player;
	}

	public void addAmmunition(int amount) {
		ammunition.setAmount(Math.min(ammunition.getAmount() + amount, 64));
		player.getInventory().setItem(1, ammunition);
		player.updateInventory();
	}

	public int getAmmunition() {
		return ammunition.getAmount();
	}

	public void removeAmmunition(int amount) {
		ammunition.setAmount(Math.max(ammunition.getAmount() - amount, 0));
		player.getInventory().setItem(1, ammunition);
		player.updateInventory();
	}

	public void doDisguise(Match match) {
		if (disguised) {
			return;
		}
		ArrayList<Player> players = new ArrayList<>(match.getInklings().size());
		for (Inkling inkling : match.getInklings()) {
			players.add(inkling.getPlayer());
		}
		//DisguiseAPI.disguiseToPlayers(player, new MobDisguise(DisguiseType.SQUID, true), players.toArray(new Player[0]));
		player.setWalkSpeed(old_walkspeed * 1.5f);
		disguised = true;
	}

	public void doUndisguise() {
		if (!disguised) {
			return;
		}
		//DisguiseAPI.undisguiseToAll(player);
		player.setWalkSpeed(old_walkspeed);
		disguised = false;
	}

	public UUID getID() {
		return id;
	}

	public TeamColor getTeam() {
		return team;
	}

	public Long getMatchId() {
		return match_id;
	}

	public SquidClass getSquidClass() {
		return squid_class;
	}

	public void setSquidClass(SquidClass squid_class) {
		this.squid_class = squid_class;
		player.getInventory().clear();
		ItemStack gun = squid_class.cloneGunItem(team);
		ItemMeta meta = gun.getItemMeta();
		List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>(4);
		for (int i = 0; i < lore.size(); i++) {
			lore.set(i, lore.get(i).replace("<team>", team.getTeamName()));
		}
		meta.setLore(lore);
		gun.setItemMeta(meta);
		player.getInventory().addItem(gun);
		player.getInventory().addItem(ammunition);
		ItemStack[] additional_items = squid_class.cloneAdditionalItems(team);
		if (additional_items != null) {
			player.getInventory().addItem(additional_items);
		}
		player.updateInventory();
	}

	public boolean isDead() {
		return is_dead;
	}

	public void setDead(boolean is_dead) {
		this.is_dead = is_dead;
	}

	public boolean isDisguised() {
		return disguised;
	}

	public void setHasVoted() {
		voted = true;
	}

	public boolean hasVoted() {
		return voted;
	}

	public void respawn() {
		if (respawn_timer == null) {
			respawn_timer = new RespawnTimer(this, 10);
		}
	}

	public void doRespawn() {
		respawn_timer = null;
	}

	public void resetPlayer() {
		if (respawn_timer != null) {
			respawn_timer.cancel();
			respawn_timer = null;
		}
		player.getInventory().setContents(old_inventory);
		player.getInventory().setArmorContents(old_armor);
		player.setScoreboard(old_scoreboard);
		player.setGameMode(old_gamemode);
		player.setLevel(old_level);
		player.setExp(old_exp);
		player.setWalkSpeed(old_walkspeed);
		player.updateInventory();
	}
}
