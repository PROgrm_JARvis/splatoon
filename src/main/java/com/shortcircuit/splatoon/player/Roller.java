package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.PlayerPaintBlockEvent;
import com.shortcircuit.splatoon.game.statistics.MatchStatistics;
import com.shortcircuit.splatoon.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 12/1/2015
 */
public class Roller extends SquidClass {
	private static final ItemStack INK_ROLLER = new ItemStack(Material.WOOL, 1);

	static {
		ItemMeta meta = INK_ROLLER.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.roller.tool", String.class, "&d&oInk roller")));
		LinkedList<String> lore = new LinkedList<>();
		lore.add(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.tool.lore", String.class, "&dSpreads <team> ink")));
		meta.setLore(lore);
		INK_ROLLER.setItemMeta(meta);
	}

	public Roller() {
		super(ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig()
				.getNode("inkling.roller.name", String.class, "&eRoller")));
	}

	@Override
	public ItemStack cloneGunItem(TeamColor team) {
		ItemStack clone = INK_ROLLER.clone();
		clone.setDurability(team.getColorData());
		return clone;
	}

	@Override
	public String getDescription() {
		return Splatoon.getInstance().getLangConfig().getNode("inkling.roller.desc", String.class,
				"This class spreads ink wherever it goes, painting a small area under its feet");
	}

	@EventHandler
	public void splat(final PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (event.getFrom().distanceSquared(event.getTo()) == 0.0
				|| player.getItemInHand() == null
				|| !player.getItemInHand().getType().equals(Material.WOOL)) {
			return;
		}
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if(inkling == null || inkling.isDead() || inkling.getAmmunition() < 2 ||  !(inkling.getSquidClass() instanceof Roller)
				|| !Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId()).getMatchHandler().isMatchStarted()) {
			return;
		}
		Match match = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getID()).getMatchHandler().getCurrentMatch();
		MatchStatistics statistics = match.getStatistics();
		TeamColor team = inkling.getTeam();
		byte new_color = team.getColorData();
		Location origin = player.getLocation().subtract(1, 1, 1);
		Block neighbor;
		inkling.removeAmmunition(2);
		for (int x = 0; x < 3; x++) {
			for (int z = 0; z < 3; z++) {
				neighbor = origin.getBlock();
				if (neighbor != null && Utils.getPaintableSurfaces().contains(neighbor.getType())) {
					byte old_color = neighbor.getData();
					if (old_color != new_color) {
						neighbor.setData(new_color);
						statistics.increment(team);
						if (TeamColor.getTeam(old_color) != null) {
							statistics.decrement(TeamColor.getTeam(old_color));
						}
						Bukkit.getServer().getPluginManager().callEvent(new PlayerPaintBlockEvent(inkling, match, neighbor, TeamColor.getTeam(old_color), team));
					}
					statistics.update();
					neighbor.getState().update();
				}
				origin = origin.add(0, 0, 1);
			}
			origin = origin.add(1, 0, -3);
		}
	}
}
