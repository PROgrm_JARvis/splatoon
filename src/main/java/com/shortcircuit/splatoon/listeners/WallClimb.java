package com.shortcircuit.splatoon.listeners;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.util.Utils;
import com.shortcircuit.utils.bukkit.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class WallClimb implements Listener {

	public WallClimb() {
	}

	@EventHandler
	public void wallClimb(final PlayerMoveEvent event) {
		if (event.getFrom().distanceSquared(event.getTo()) == 0.0) {
			return;
		}
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(event.getPlayer().getUniqueId());
		if (inkling == null || inkling.isDead() || !inkling.isDisguised()) {
			return;
		}
		BlockFace face = PlayerUtils.getCardinalDirection(event.getTo());
		byte team_color = inkling.getTeam().getColorData();
		Block wall = event.getTo().getWorld().getBlockAt(event.getTo()).getRelative(face);
		if (wall == null
				|| !wall.getType().isSolid()
				|| !Utils.getPaintableSurfaces().contains(wall.getType())
				|| wall.getData() != team_color) {
			return;
		}

		// Spoof the vines
		Block block = event.getTo().getBlock();
		if (block == null || block.getType() != Material.AIR) {
			return;
		}
		inkling.getPlayer().sendBlockChange(event.getTo(), Material.VINE, getVineData(face));
		inkling.getPlayer().setFallDistance(0);
		new BlockGetterRidderOf(block.getLocation());
	}

	private byte getVineData(BlockFace face) {
		switch (face) {
			case NORTH_WEST:
				//return 9;
			case NORTH:
				return 1;
			case NORTH_EAST:
				//return 3;
			case EAST:
				return 2;
			case SOUTH_EAST:
				//return 6;
			case SOUTH:
				return 4;
			case SOUTH_WEST:
				//return 12;
			case WEST:
				return 8;

		}
		return 0;
	}

	private class BlockGetterRidderOf implements Runnable {
		private final Location location;

		private BlockGetterRidderOf(Location locaton) {
			this.location = locaton;
			Bukkit.getScheduler().scheduleAsyncDelayedTask(Splatoon.getInstance(), this, 40);
		}

		@Override
		public void run() {
			location.getBlock().getState().update();
		}
	}
}
