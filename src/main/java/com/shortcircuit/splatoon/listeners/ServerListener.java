package com.shortcircuit.splatoon.listeners;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 1/21/2017.
 */
public class ServerListener implements Listener{
	private final Splatoon splatoon = Splatoon.getInstance();

	@EventHandler
	public void onPlayerKick(final PlayerKickEvent event){
		Arena arena = splatoon.getArenaManager().getArenaContainingInkling(event.getPlayer().getUniqueId());
		if(arena != null && arena.getMatchHandler().hasMatch()){
			arena.getMatchHandler().getCurrentMatch().removeInkling(event.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void onPlayerLeave(final PlayerQuitEvent event){
		Arena arena = splatoon.getArenaManager().getArenaContainingInkling(event.getPlayer().getUniqueId());
		if(arena != null && arena.getMatchHandler().hasMatch()){
			arena.getMatchHandler().getCurrentMatch().removeInkling(event.getPlayer().getUniqueId());
		}
	}
}
