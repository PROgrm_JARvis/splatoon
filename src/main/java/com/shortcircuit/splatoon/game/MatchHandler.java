package com.shortcircuit.splatoon.game;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class MatchHandler {
	private final Arena arena;
	private Match current_match;

	public MatchHandler(Arena arena) {
		this.arena = arena;
	}

	public void newMatch() {
		if(current_match != null){
			return;
		}
		current_match = new Match(arena);
	}

	public Match getCurrentMatch() {
		return current_match;
	}

	public boolean hasMatch(){
		return current_match != null;
	}

	public boolean isMatchStarted(){
		return current_match != null && current_match.hasStarted();
	}

	protected void reset(){
		current_match = null;
		arena.getStatusSign().updateMatchState(0);
	}
}
