package com.shortcircuit.splatoon.game.timers;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.MatchEndEvent;
import com.shortcircuit.splatoon.game.statistics.SplatSign;
import com.shortcircuit.splatoon.player.Inkling;
import org.bukkit.Bukkit;

/**
 * @author ShortCircuit908
 */
public class MatchTimer implements Runnable {
	private final SplatSign status_sign;
	private final Match match;
	private final int task_id;
	private final int max_time = 180;
	private int seconds = max_time;

	public MatchTimer(Match match, SplatSign status_sign) {
		this.status_sign = status_sign;
		this.match = match;
		task_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 0, 20);
	}

	@Override
	public void run() {
		if (seconds <= 0) {
			stop();
			return;
		}
		status_sign.updateMatchState(2);
		status_sign.updateTime(seconds);
		for (Inkling inkling : match.getInklings()) {
			inkling.getPlayer().setLevel(seconds);
			inkling.getPlayer().setExp((float) getPercentage());
		}
		seconds--;
	}

	public void stop() {
		match.stopMatch();
		Bukkit.getServer().getScheduler().cancelTask(task_id);
		Bukkit.getServer().getPluginManager().callEvent(new MatchEndEvent(match, MatchEndEvent.Cause.TIMER));
	}

	public void cancel(){
		Bukkit.getServer().getScheduler().cancelTask(task_id);
	}

	public int getRemainingTime() {
		return seconds;
	}

	public double getPercentage() {
		return ((double) seconds) / ((double) max_time);
	}
}
