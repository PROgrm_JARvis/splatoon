package com.shortcircuit.splatoon.game.timers;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.game.statistics.MatchStatistics;
import com.shortcircuit.splatoon.listeners.SplatListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.event.Listener;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.UUID;

/**
 * @author ShortCircuit908
 */
public class ChargerTimer implements Runnable, Listener {
	private final Inkling inkling;
	private final Match match;
	private final Arrow arrow;
	private final MatchStatistics statistics;
	private final TeamColor team;
	private final int task_id;

	public ChargerTimer(Inkling inkling, Match match, Arrow arrow, MatchStatistics statistics, TeamColor team) {
		this.inkling = inkling;
		this.match = match;
		this.arrow = arrow;
		this.statistics = statistics;
		this.team = team;
		this.task_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 0, 0);
	}

	@Override
	public void run() {
		if (arrow.isDead() || !arrow.isValid()) {
			cancel();
			return;
		}
		Block block = getBlockBeneath(arrow.getLocation());
		SplatListener.splat(inkling, match, block, team, statistics);
	}


	private Block getBlockBeneath(Location location) {
		BlockIterator iterator = new BlockIterator(location.getWorld(), location.toVector(), new Vector(0, -1, 0), 0, 64);
		while (iterator.hasNext()) {
			Block block = iterator.next();
			if (block == null || !block.getType().isSolid()) {
				continue;
			}
			return block;
		}
		return null;
	}

	public UUID getArrowID() {
		return arrow.getUniqueId();
	}

	public void cancel() {
		Bukkit.getServer().getScheduler().cancelTask(task_id);
	}
}
