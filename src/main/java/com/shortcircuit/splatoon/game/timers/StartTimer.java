package com.shortcircuit.splatoon.game.timers;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.MatchStartEvent;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.util.AnnouncementHandler;
import com.shortcircuit.utils.bukkit.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author ShortCircuit908
 */
public class StartTimer implements Runnable {
	private final Match match;
	private final Arena arena;
	private final int task_id;
	private int seconds = 60;

	public StartTimer(Match match, Arena arena) {
		this.match = match;
		this.arena = arena;
		task_id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 0, 20);
	}

	@Override
	public void run() {
		if (seconds <= 0) {
			if (BukkitUtils.isTitleSupported()) {
				for (Inkling inkling : arena.getMatchHandler().getCurrentMatch().getInklings()) {
					inkling.getPlayer().sendTitle(ChatColor.GOLD + "START", "");
				}
			}
			stop();
			return;
		}
		if (seconds % 15 == 0) {
			if(Splatoon.getInstance().getJsonConfig().getNode("global_announcements", boolean.class, true)) {
				AnnouncementHandler.announceExcludeArena("match.start.timer.others", arena.getID(), seconds);
			}
			AnnouncementHandler.announceArenaOnly("match.start.timer", arena, seconds);
			if (BukkitUtils.isTitleSupported()) {
				for (Inkling inkling : arena.getMatchHandler().getCurrentMatch().getInklings()) {
					inkling.getPlayer().sendTitle(ChatColor.GREEN + "" + seconds + "s", "");
				}
			}
		}
		else if (seconds <= 10) {
			AnnouncementHandler.announceArenaOnly("match.start.timer", arena, seconds);
			if (BukkitUtils.isTitleSupported()) {
				for (Inkling inkling : arena.getMatchHandler().getCurrentMatch().getInklings()) {
					inkling.getPlayer().sendTitle(getCountColor(seconds) + "" + seconds + "s", "");
				}
			}
		}
		seconds--;
	}

	public void stop() {
		match.doStartMatch();
		Bukkit.getServer().getPluginManager().callEvent(new MatchStartEvent(match, MatchStartEvent.Cause.TIMER));
		Bukkit.getScheduler().cancelTask(task_id);
	}

	public void cancel() {
		Bukkit.getScheduler().cancelTask(task_id);
	}

	private ChatColor getCountColor(int count) {
		if (count > 5) {
			return ChatColor.GREEN;
		}
		if (count > 2) {
			return ChatColor.YELLOW;
		}
		return ChatColor.RED;
	}
}
