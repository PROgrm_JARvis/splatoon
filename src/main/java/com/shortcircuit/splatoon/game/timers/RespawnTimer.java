package com.shortcircuit.splatoon.game.timers;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.utils.bukkit.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 * @author ShortCircuit908
 */
public class RespawnTimer implements Runnable {
	public static final GameMode SPECTATOR;
	private final Inkling inkling;
	private int seconds = 10;
	private final int task_id;
	private final Arena arena;
	private final Match match;
	private final GameMode old_mode;
	private final Objective respawn_objective;
	private final Scoreboard old_scoreboard;
	private final PotionEffect invisibility_effect;

	static {
		GameMode spectator = null;
		try {
			spectator = GameMode.valueOf("SPECTATOR");
		}
		catch (Exception e) {
			// Do nothing
		}
		SPECTATOR = spectator;
	}

	public RespawnTimer(Inkling inkling, int max_seconds) {
		task_id = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 0, 20);
		inkling.setDead(true);
		this.inkling = inkling;
		old_scoreboard = inkling.getPlayer().getScoreboard();
		old_mode = inkling.getPlayer().getGameMode();
		invisibility_effect = new PotionEffect(PotionEffectType.INVISIBILITY, 99999, 1);
		inkling.getPlayer().addPotionEffect(invisibility_effect);
		if(SPECTATOR != null) {
			inkling.getPlayer().setGameMode(SPECTATOR);
		}
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		respawn_objective = scoreboard.registerNewObjective(ChatColor.GOLD + "Respawn", "dummy");
		respawn_objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		inkling.getPlayer().setScoreboard(scoreboard);
		arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getPlayer().getUniqueId());
		if (arena == null || !arena.getMatchHandler().isMatchStarted()) {
			match = null;
			return;
		}
		match = arena.getMatchHandler().getCurrentMatch();
		if (match == null) {
			Bukkit.getServer().getScheduler().cancelTask(task_id);
			return;
		}
		seconds = max_seconds;
	}

	@Override
	public void run() {
		if (seconds <= 0) {
			if (BukkitUtils.isTitleSupported()) {
				inkling.getPlayer().resetTitle();
			}
			respawn();
			return;
		}
		respawn_objective.getScore(ChatColor.GOLD + "Respawn in:").setScore(seconds);
		if (BukkitUtils.isTitleSupported()) {
			inkling.getPlayer().sendTitle(ChatColor.GOLD + "Respawn in " + seconds, "");
		}
		seconds--;
	}

	private void respawn() {
		Bukkit.getServer().getScheduler().cancelTask(task_id);
		//Match has already ended
		if (!match.getArena().getMatchHandler().hasMatch()) {
			inkling.resetPlayer();
			return;
		}
		inkling.setDead(false);
		inkling.getPlayer().setGameMode(old_mode);
		inkling.getPlayer().setScoreboard(old_scoreboard);
		inkling.getPlayer().setHealth(inkling.getPlayer().getMaxHealth());
		inkling.getPlayer().teleport(arena.getTeamSpawn(inkling.getTeam()));
		inkling.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
		inkling.doRespawn();
	}

	public void cancel() {
		Bukkit.getServer().getScheduler().cancelTask(task_id);
	}
}
