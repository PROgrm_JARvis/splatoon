package com.shortcircuit.splatoon.game;

import com.google.common.base.Joiner;
import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.event.*;
import com.shortcircuit.splatoon.game.statistics.MatchStatistics;
import com.shortcircuit.splatoon.game.timers.DamageTimer;
import com.shortcircuit.splatoon.game.timers.MatchTimer;
import com.shortcircuit.splatoon.game.timers.StartTimer;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.util.AnnouncementHandler;
import com.shortcircuit.splatoon.util.SchematicUtil;
import com.shortcircuit.splatoon.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.*;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Match {
	private final Arena arena;
	private boolean started = false;
	private final ArrayList<Inkling> inklings = new ArrayList<>();
	private final MatchStatistics statistics;
	private DamageTimer damage_timer;
	private MatchTimer timer;
	private StartTimer start_timer;
	private int start_votes = 0;
	public static final int MAX_PLAYERS;

	static {
		MAX_PLAYERS = Splatoon.getInstance().getJsonConfig().getNode("max_players", int.class, 32);
	}

	public Match(Arena arena) {
		this.arena = arena;
		SchematicUtil.regenArena(arena);
		statistics = new MatchStatistics(arena, this);
		if (statistics.getTotalBlocks() == 0) {
			AnnouncementHandler.announceExcludeArena("arena.invalid", arena.getID());
			Bukkit.getScheduler().scheduleSyncDelayedTask(Splatoon.getInstance(), new Runnable() {
				@Override
				public void run() {
					stopMatch();
					Bukkit.getServer().getPluginManager().callEvent(new MatchEndEvent(Match.this, MatchEndEvent.Cause.MATCH_WON));
				}
			}, 0L);
			return;
		}
		arena.getStatusSign().updateMatchState(1);
		if (Splatoon.getInstance().getJsonConfig().getNode("global_announcements", boolean.class, true)) {
			AnnouncementHandler.announceExcludeArena("match.new.1", arena.getID());
			AnnouncementHandler.announceExcludeArena("match.new.2", arena.getID());
		}
	}

	public void addInkling(Player player, TeamColor team, SquidClass squid_class) {
		if (reachedPlayerCap()) {
			AnnouncementHandler.announceUserOnly("match.full", player);
			return;
		}
		if (!arena.getEnabledTeams().contains(team)) {
			AnnouncementHandler.announceUserOnly("match.invalid_team", player, team.getTeamName());
			return;
		}
		Inkling inkling = new Inkling(player, team, squid_class, arena.getID());
		inklings.add(inkling);
		statistics.addScoreboard(inkling);
		AnnouncementHandler.announceArenaOnly("match.player.join", arena, player.getName(), team.getTeamName());
		if (getTeamsFilled() >= 2) {
			startMatch();
		}
		arena.getStatusSign().updatePlayers(inklings.size());
		Bukkit.getServer().getPluginManager().callEvent(new PlayerJoinMatchEvent(inkling, this));
	}

	public void removeInkling(UUID id) {
		Inkling inkling = getInkling(id);
		removeInkling(inkling);
	}

	public void removeInkling(Inkling inkling) {
		if (inkling != null) {
			Player player = inkling.getPlayer();
			inkling.setDead(false);
			AnnouncementHandler.announceArenaOnly("match.player.leave", arena, player.getName());
			inklings.remove(inkling);
			inkling.getPlayer().eject();
			player.teleport(arena.getExitPos());
			inkling.doUndisguise();
			inkling.resetPlayer();
			arena.getStatusSign().updatePlayers(inklings.size());
			if (inklings.size() <= 1) {
				if (inklings.size() == 0) {
					if (timer != null) {
						timer.stop();
					}
					else {
						arena.getMatchHandler().reset();
					}
				}
				if (start_timer != null) {
					start_timer.cancel();
					start_timer = null;
				}
			}
			Bukkit.getPluginManager().callEvent(new PlayerLeaveMatchEvent(inkling, this));
		}
	}

	public Inkling getInkling(UUID id) {
		for (Inkling inkling : inklings) {
			if (inkling.getID().equals(id)) {
				return inkling;
			}
		}
		return null;
	}

	public ArrayList<Inkling> getInklings() {
		return inklings;
	}

	public void startMatch() {
		if (start_timer == null) {
			start_timer = new StartTimer(this, arena);
		}
	}

	public void doStartMatch() {
		if (started) {
			return;
		}
		if (start_timer != null) {
			start_timer.cancel();
			start_timer = null;
		}
		arena.getStatusSign().updateMatchState(2);
		for (Inkling inkling : inklings) {
			inkling.getPlayer().teleport(arena.getTeamSpawn(inkling.getTeam()));
		}
		AnnouncementHandler.announceArenaOnly("match.start", arena);
		if(Splatoon.getInstance().getJsonConfig().getNode("global_announcements", boolean.class, true)) {
			AnnouncementHandler.announceExcludeArena("match.start.others", arena.getID());
		}
		started = true;
		timer = new MatchTimer(this, arena.getStatusSign());
		damage_timer = new DamageTimer(this);
	}

	public void stopMatch() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if(Splatoon.getInstance().getJsonConfig().getNode("global_announcements", boolean.class, true)) {
			AnnouncementHandler.announceExcludeArena("match.end.others", arena.getID());
		}
		arena.getStatusSign().updateMatchState(0);
		AnnouncementHandler.announceArenaOnly("match.end", arena);
		if (statistics.getTotalBlocks() > 0) {
			displayFinalScores();
			if (Splatoon.getInstance().isEnabled()) {
				spawnFireworks(arena.getExitPos());
			}
		}
		ArrayList<Inkling> to_remove = new ArrayList<>(inklings.size());
		to_remove.addAll(inklings);
		for (Inkling inkling : to_remove) {
			removeInkling(inkling);
		}
		to_remove.clear();
		arena.getMatchHandler().reset();
		if (damage_timer != null) {
			damage_timer.cancel();
		}
		SchematicUtil.regenArena(arena);
	}

	public boolean hasStarted() {
		return started;
	}

	public LinkedList<TeamColor> getDeficientTeams() {
		LinkedList<DeficiencyReport<TeamColor>> reports = new LinkedList<>();
		LinkedList<TeamColor> deficient = new LinkedList<>();
		for (TeamColor team : TeamColor.values()) {
			if (arena.getEnabledTeams().contains(team)) {
				reports.add(new DeficiencyReport<>(team, getNumPlayers(team)));
			}
		}
		Collections.sort(reports);
		int min = reports.getFirst().quantity;
		for (DeficiencyReport<TeamColor> report : reports) {
			if (report.quantity == min) {
				deficient.add(report.identifier);
			}
		}
		return deficient;
	}

	public TeamColor getDeficientTeam() {
		LinkedList<TeamColor> deficient = getDeficientTeams();
		if (deficient.size() == 1) {
			return deficient.getFirst();
		}
		return deficient.get(new Random(System.currentTimeMillis()).nextInt(deficient.size()));
	}

	public int getTeamsFilled() {
		int teams_filled = 0;
		boolean red_counted = false;
		boolean yellow_counted = false;
		boolean green_counted = false;
		boolean blue_counted = false;
		for (Inkling inkling : inklings) {
			switch (inkling.getTeam()) {
				case RED:
					if (!arena.getEnabledTeams().contains(TeamColor.RED)) {
						break;
					}
					if (!red_counted) {
						red_counted = true;
						teams_filled++;
					}
					break;
				case YELLOW:
					if (!arena.getEnabledTeams().contains(TeamColor.YELLOW)) {
						break;
					}
					if (!yellow_counted) {
						yellow_counted = true;
						teams_filled++;
					}
					break;
				case GREEN:
					if (!arena.getEnabledTeams().contains(TeamColor.GREEN)) {
						break;
					}
					if (!green_counted) {
						green_counted = true;
						teams_filled++;
					}
					break;
				case BLUE:
					if (!arena.getEnabledTeams().contains(TeamColor.BLUE)) {
						break;
					}
					if (!blue_counted) {
						blue_counted = true;
						teams_filled++;
					}
					break;
			}
		}
		return teams_filled;
	}

	public LinkedList<SquidClass> getDeficientClasses() {
		LinkedList<DeficiencyReport<SquidClass>> reports = new LinkedList<>();
		LinkedList<SquidClass> deficient = new LinkedList<>();
		for (SquidClass squid_class : SquidClass.getRegisteredClasses()) {
			reports.add(new DeficiencyReport<>(squid_class, getNumClass(squid_class)));
		}
		Collections.sort(reports);
		int min = reports.get(0).quantity;
		for (DeficiencyReport<SquidClass> report : reports) {
			if (report.quantity == min) {
				deficient.add(report.identifier);
			}
		}
		return deficient;
	}

	private class DeficiencyReport<T> implements Comparable<DeficiencyReport<T>> {
		private final T identifier;
		private final int quantity;

		public DeficiencyReport(T identifier, int quantity) {
			this.identifier = identifier;
			this.quantity = quantity;
		}

		@Override
		public int compareTo(DeficiencyReport<T> o) {
			return quantity - o.quantity;
		}
	}

	public SquidClass getDeficientClass() {
		LinkedList<SquidClass> deficient = getDeficientClasses();
		if (deficient.size() == 1) {
			return deficient.getFirst();
		}
		return deficient.get(new Random(System.currentTimeMillis()).nextInt(deficient.size()));
	}

	public int getNumClass(SquidClass squid_class) {
		int count = 0;
		for (Inkling inkling : inklings) {
			if (inkling.getSquidClass().equals(squid_class)) {
				count++;
			}
		}
		return count;
	}

	public int getNumPlayers(TeamColor team) {
		int count = 0;
		for (Inkling inkling : inklings) {
			if (inkling.getTeam().equals(team)) {
				count++;
			}
		}
		return count;
	}

	public MatchStatistics getStatistics() {
		return statistics;
	}

	public MatchTimer getTimer() {
		return timer;
	}

	private void displayFinalScores() {
		LinkedList<MatchStatistics.TeamScore> winners = statistics.getWinningTeams();
		ArrayList<String> winners_names = new ArrayList<>(winners.size());
		for (MatchStatistics.TeamScore winner : winners) {
			winners_names.add(winner.getTeam().getTeamName());
		}
		String max_score = ((int) (Math.round(winners.getFirst().getScore() * 100.0)) / 100.0) + "";
		AnnouncementHandler.announceArenaOnly("match.win", arena, Joiner.on(", ").join(winners_names), max_score);
		LinkedList<Inkling> winning_players = new LinkedList<>();
		for (Inkling inkling : getInklings()) {
			for (MatchStatistics.TeamScore winning_score : winners) {
				if (winning_score.getTeam().equals(inkling.getTeam())) {
					winning_players.add(inkling);
					break;
				}
			}
		}
		Bukkit.getPluginManager().callEvent(new MatchWinEvent(this, winners, winning_players));
	}

	private void spawnFireworks(Location location) {
		ArrayList<Color> colors = new ArrayList<>();
		for (MatchStatistics.TeamScore score : statistics.getWinningTeams()) {
			colors.add(Utils.teamToColor(score.getTeam().getColorData()));
		}
		new FireworkSpawner(location, colors);
	}

	private class FireworkSpawner implements Runnable {
		private final Location location;
		private final FireworkEffect effect;
		private final int task_id;
		private int spawned = 0;

		public FireworkSpawner(Location location, List<Color> colors) {
			this.location = location;
			effect = FireworkEffect.builder().withColor(colors).withTrail().withFlicker().with(FireworkEffect.Type.BALL_LARGE).build();
			task_id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 20, 10);
		}

		@Override
		public void run() {
			Firework firework = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
			FireworkMeta meta = firework.getFireworkMeta();
			meta.addEffect(effect);
			meta.setPower(0);
			firework.setFireworkMeta(meta);
			spawned++;
			if (spawned >= 6) {
				Bukkit.getScheduler().cancelTask(task_id);
			}
		}
	}

	public void submitStartVote(Inkling inkling) {
		if (inkling.hasVoted() || started) {
			return;
		}
		if (getTeamsFilled() < 2) {
			AnnouncementHandler.announceUserOnly("match.teamreq", inkling.getPlayer());
			return;
		}
		inkling.setHasVoted();
		start_votes++;
		if (getVotePercentage() > 0.6) {
			AnnouncementHandler.announceArenaOnly("match.start.vote", arena);
			if (start_timer != null) {
				start_timer.cancel();
			}
			doStartMatch();
			Bukkit.getServer().getPluginManager().callEvent(new MatchStartEvent(this, MatchStartEvent.Cause.VOTE));
		}
	}

	private double getVotePercentage() {
		return ((double) start_votes) / ((double) inklings.size());
	}

	public boolean isStarting() {
		return start_timer != null;
	}

	public Arena getArena() {
		return arena;
	}

	public boolean reachedPlayerCap() {
		return inklings.size() >= MAX_PLAYERS;
	}
}