package com.shortcircuit.splatoon.game.event;

import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.statistics.MatchStatistics;
import com.shortcircuit.splatoon.player.Inkling;

import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public class MatchWinEvent extends SplatoonEvent {
	private final Match match;
	private final LinkedList<MatchStatistics.TeamScore> winning_teams;
	private final LinkedList<Inkling> winning_players;

	public MatchWinEvent(Match match, LinkedList<MatchStatistics.TeamScore> winning_teams, LinkedList<Inkling> winning_players) {
		this.match = match;
		this.winning_teams = winning_teams;
		this.winning_players = winning_players;
	}

	public Match getMatch() {
		return match;
	}

	public LinkedList<MatchStatistics.TeamScore> getWinningTeams() {
		return winning_teams;
	}

	public LinkedList<Inkling> getWinningPlayers() {
		return winning_players;
	}
}
