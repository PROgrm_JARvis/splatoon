package com.shortcircuit.splatoon.game.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public abstract class SplatoonEvent extends Event {
	private static final HandlerList handler_list = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handler_list;
	}

	public static HandlerList getHandlerList() {
		return handler_list;
	}
}
