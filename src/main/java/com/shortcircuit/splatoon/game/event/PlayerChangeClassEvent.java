package com.shortcircuit.splatoon.game.event;

import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.SquidClass;
import org.bukkit.event.Cancellable;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public class PlayerChangeClassEvent extends SplatoonEvent implements Cancellable {
	private boolean cancelled = false;
	private final Inkling inkling;
	private final Match match;
	private final SquidClass old_class;
	private SquidClass new_class;

	public PlayerChangeClassEvent(Inkling inkling, Match match, SquidClass old_class, SquidClass new_class) {
		this.inkling = inkling;
		this.match = match;
		this.old_class = old_class;
		this.new_class = new_class;
	}

	public Inkling getInkling() {
		return inkling;
	}

	public Match getMatch() {
		return match;
	}

	public SquidClass getOldClass() {
		return old_class;
	}

	public SquidClass getNewClass() {
		return new_class;
	}

	public synchronized void setNewClass(SquidClass new_class) {
		this.new_class = new_class;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public synchronized void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}