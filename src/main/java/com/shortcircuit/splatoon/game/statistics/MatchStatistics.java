package com.shortcircuit.splatoon.game.statistics;

import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class MatchStatistics {
	private long total_blocks;
	private final boolean red_enabled;
	private long red_blocks;
	private final boolean green_enabled;
	private long green_blocks;
	private final boolean yellow_enabled;
	private long yellow_blocks;
	private final boolean blue_enabled;
	private long blue_blocks;
	private final Match match;
	private final ArrayList<ScoreboardData> scoreboards = new ArrayList<>();

	public MatchStatistics(Arena arena, Match match) {
		this.match = match;
		red_enabled = arena.getEnabledTeams().contains(TeamColor.RED);
		green_enabled = arena.getEnabledTeams().contains(TeamColor.GREEN);
		yellow_enabled = arena.getEnabledTeams().contains(TeamColor.YELLOW);
		blue_enabled = arena.getEnabledTeams().contains(TeamColor.BLUE);
		Block block;
		for (int x = arena.getMinPos().getBlockX(); x <= arena.getMaxPos().getX(); x++) {
			for (int z = arena.getMinPos().getBlockZ(); z <= arena.getMaxPos().getZ(); z++) {
				for (int y = arena.getMinPos().getBlockY(); y <= arena.getMaxPos().getY(); y++) {
					block = arena.getWorld().getBlockAt(x, y, z);
					if (block != null && Utils.getPaintableSurfaces().contains(block.getType())) {
						total_blocks++;
						if (!Utils.isColorableSurface(block.getType())) {
							continue;
						}
						byte data = block.getData();
						if (data == TeamColor.RED.getColorData() && red_enabled) {
							red_blocks++;
						}
						else if (data == TeamColor.YELLOW.getColorData() && yellow_enabled) {
							yellow_blocks++;
						}
						else if (data == TeamColor.GREEN.getColorData() && green_enabled) {
							green_blocks++;
						}
						else if (data == TeamColor.BLUE.getColorData() && blue_enabled) {
							blue_blocks++;
						}
					}
				}
			}
		}
	}

	public long getTotalBlocks() {
		return total_blocks;
	}

	public void increment(TeamColor team) {
		switch (team) {
			case RED:
				if (!red_enabled) {
					return;
				}
				red_blocks++;
				if (red_blocks == total_blocks) {
					match.getTimer().stop();
				}
				break;
			case YELLOW:
				if (!yellow_enabled) {
					return;
				}
				yellow_blocks++;
				if (yellow_blocks == total_blocks) {
					match.getTimer().stop();
				}
				break;
			case GREEN:
				if (!green_enabled) {
					return;
				}
				green_blocks++;
				if (green_blocks == total_blocks) {
					match.getTimer().stop();
				}
				break;
			case BLUE:
				if (!blue_enabled) {
					return;
				}
				blue_blocks++;
				if (blue_blocks == total_blocks) {
					match.getTimer().stop();
				}
				break;
		}
	}

	public void decrement(TeamColor team) {
		switch (team) {
			case RED:
				if (!red_enabled) {
					return;
				}
				red_blocks--;
				break;
			case YELLOW:
				if (!yellow_enabled) {
					return;
				}
				yellow_blocks--;
				break;
			case GREEN:
				if (!green_enabled) {
					return;
				}
				green_blocks--;
				break;
			case BLUE:
				if (!blue_enabled) {
					return;
				}
				blue_blocks--;
				break;
		}
	}

	public long getBlockCount(TeamColor team) {
		switch (team) {
			case RED:
				return red_blocks;
			case YELLOW:
				return yellow_blocks;
			case GREEN:
				return green_blocks;
			case BLUE:
				return blue_blocks;
		}
		return 0;
	}

	public double getBlockPercentage(TeamColor team) {
		return ((double) getBlockCount(team) / (double) total_blocks) * 100.0;
	}

	public void addScoreboard(Inkling inkling) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		ScoreboardData data = new ScoreboardData(scoreboard);
		scoreboards.add(data);
		inkling.getPlayer().setScoreboard(scoreboard);
	}

	public void update() {
		for (ScoreboardData data : scoreboards) {
			data.update();
		}
		if (red_enabled) {
			match.getArena().getStatusSign().updateScores((int) Math.round(getBlockPercentage(TeamColor.RED)), TeamColor.RED);
		}
		if (yellow_enabled) {
			match.getArena().getStatusSign().updateScores((int) Math.round(getBlockPercentage(TeamColor.YELLOW)), TeamColor.YELLOW);
		}
		if (green_enabled) {
			match.getArena().getStatusSign().updateScores((int) Math.round(getBlockPercentage(TeamColor.GREEN)), TeamColor.GREEN);
		}
		if (blue_enabled) {
			match.getArena().getStatusSign().updateScores((int) Math.round(getBlockPercentage(TeamColor.BLUE)), TeamColor.BLUE);
		}
	}

	private class ScoreboardData {
		private final Scoreboard scoreboard;
		private final Objective score_objective;

		private ScoreboardData(Scoreboard scoreboard) {
			this.scoreboard = scoreboard;
			score_objective = scoreboard.registerNewObjective(ChatColor.GOLD + "% Progress", "dummy");
			score_objective.setDisplaySlot(DisplaySlot.SIDEBAR);
			update();

		}

		public void update() {
			if (red_enabled) {
				score_objective.getScore(TeamColor.RED.getTeamName() + " Ink").setScore((int) Math.round(getBlockPercentage(TeamColor.RED)));
			}
			if (yellow_enabled) {
				score_objective.getScore(TeamColor.YELLOW.getTeamName() + " Ink").setScore((int) Math.round(getBlockPercentage(TeamColor.YELLOW)));
			}
			if (green_enabled) {
				score_objective.getScore(TeamColor.GREEN.getTeamName() + " Ink").setScore((int) Math.round(getBlockPercentage(TeamColor.GREEN)));
			}
			if (blue_enabled) {
				score_objective.getScore(TeamColor.BLUE.getTeamName() + " Ink").setScore((int) Math.round(getBlockPercentage(TeamColor.BLUE)));
			}
		}
	}

	public LinkedList<TeamScore> getWinningTeams() {
		LinkedList<TeamScore> scores = new LinkedList<>();
		if (red_enabled) {
			scores.add(new TeamScore(TeamColor.RED, getBlockPercentage(TeamColor.RED)));
		}
		if (yellow_enabled) {
			scores.add(new TeamScore(TeamColor.YELLOW, getBlockPercentage(TeamColor.YELLOW)));
		}
		if (green_enabled) {
			scores.add(new TeamScore(TeamColor.GREEN, getBlockPercentage(TeamColor.GREEN)));
		}
		if (blue_enabled) {
			scores.add(new TeamScore(TeamColor.BLUE, getBlockPercentage(TeamColor.BLUE)));
		}
		Collections.sort(scores);
		double max = scores.getFirst().score;
		LinkedList<TeamScore> winners = new LinkedList<>();
		for (TeamScore score : scores) {
			if (score.score == max) {
				winners.add(score);
			}
		}
		return winners;
	}


	public class TeamScore implements Comparable<TeamScore> {
		private final TeamColor team;
		private final double score;

		public TeamScore(TeamColor team, double score) {
			this.team = team;
			this.score = score;
		}

		@Override
		public int compareTo(TeamScore o) {
			if (o.score < score) {
				return -1;
			}
			if (o.score > score) {
				return 1;
			}
			return 0;
		}

		public TeamColor getTeam() {
			return team;
		}

		public double getScore() {
			return score;
		}
	}
}
