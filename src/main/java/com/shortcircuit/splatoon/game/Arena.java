package com.shortcircuit.splatoon.game;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.event.MatchEndEvent;
import com.shortcircuit.splatoon.game.statistics.LobbySign;
import com.shortcircuit.splatoon.game.statistics.SplatSign;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.util.SchematicUtil;
import com.shortcircuit.utils.bukkit.LocationWrapper;
import com.shortcircuit.utils.json.Exclude;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Arena {
	@Exclude
	private MatchHandler match_handler = new MatchHandler(this);
	private final int id;
	private final UUID world_id;
	private final LocationWrapper min_pos;
	private final LocationWrapper max_pos;
	private final LocationWrapper[] team_spawns = new LocationWrapper[4];
	private LocationWrapper exit_pos;
	private SplatSign status_sign;
	private ArrayList<TeamColor> enabled_teams = new ArrayList<>(4);
	private ArrayList<LobbySign> lobby_signs = new ArrayList<>();
	private boolean enabled = true;

	public Arena(Location min_pos, Location max_pos) {
		this.id = getLowestAvailableID();
		this.world_id = min_pos.getWorld().getUID();
		this.min_pos = new LocationWrapper(min_pos);
		this.max_pos = new LocationWrapper(max_pos);
		team_spawns[0] = new LocationWrapper(min_pos.getWorld().getUID(), min_pos.getBlockX() + 1.5, min_pos.getBlockY() + 1.5, min_pos.getBlockZ() + 1.5, 0, 0);
		team_spawns[1] = new LocationWrapper(min_pos.getWorld().getUID(), min_pos.getBlockX() + 1.5, min_pos.getBlockY() + 1.5, max_pos.getBlockZ() - 1.5, 0, 0);
		team_spawns[2] = new LocationWrapper(min_pos.getWorld().getUID(), max_pos.getBlockX() - 1.5, min_pos.getBlockY() + 1.5, max_pos.getBlockZ() - 1.5, 0, 0);
		team_spawns[3] = new LocationWrapper(min_pos.getWorld().getUID(), max_pos.getBlockX() - 1.5, min_pos.getBlockY() + 1.5, min_pos.getBlockZ() + 1.5, 0, 0);
		exit_pos = new LocationWrapper(min_pos.getWorld().getSpawnLocation());
		for (TeamColor team : TeamColor.values()) {
			enabled_teams.add(team);
		}
		SchematicUtil.saveArena(this);
	}

	public void setExitPos(Location exit_pos) {
		this.exit_pos = new LocationWrapper(exit_pos);
	}

	public int getID() {
		return id;
	}

	public Location getMinPos() {
		return min_pos.getLocation();
	}

	public Location getMaxPos() {
		return max_pos.getLocation();
	}

	public Location getExitPos() {
		return exit_pos.getLocation();
	}

	public World getWorld() {
		return Bukkit.getWorld(world_id);
	}

	public UUID getWorldID() {
		return world_id;
	}

	public ArrayList<TeamColor> getEnabledTeams() {
		if (enabled_teams == null) {
			enabled_teams = new ArrayList<>(4);
			for (TeamColor team : TeamColor.values()) {
				enabled_teams.add(team);
			}
		}
		return enabled_teams;
	}

	public void setEnabledTeams(Collection<TeamColor> teams) {
		if (enabled_teams == null) {
			enabled_teams = new ArrayList<>(4);
		}
		enabled_teams.clear();
		enabled_teams.addAll(teams);
	}

	public MatchHandler getMatchHandler() {
		if (match_handler == null) {
			match_handler = new MatchHandler(this);
		}
		return match_handler;
	}

	public Location getTeamSpawn(TeamColor team) {
		return team_spawns[team.ordinal()].getLocation();
	}

	public void setTeamSpawn(TeamColor team, Location spawn) {
		team_spawns[team.ordinal()] = new LocationWrapper(spawn);
	}

	public int getLowestAvailableID() {
		int id = 0;
		while (Splatoon.getInstance().getArenaManager().getArena(id) != null) {
			id++;
		}
		return id;
	}

	public SplatSign getStatusSign() {
		if (status_sign == null) {
			status_sign = new SplatSign(max_pos.getLocation(), id);
		}
		return status_sign;
	}

	public ArrayList<LobbySign> getLobbySigns() {
		if (lobby_signs == null) {
			lobby_signs = new ArrayList<>();
		}
		return lobby_signs;
	}

	public void setEnabled(boolean enabled){
		if(!enabled && getMatchHandler().hasMatch()){
			getMatchHandler().getCurrentMatch().stopMatch();
			Bukkit.getServer().getPluginManager().callEvent(new MatchEndEvent(match_handler.getCurrentMatch(), MatchEndEvent.Cause.COMMAND));
		}
		this.enabled = enabled;
		getStatusSign().updateMatchState(enabled ? 0 : 3);
	}

	public boolean isEnabled(){
		return enabled;
	}
}
