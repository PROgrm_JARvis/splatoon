package com.shortcircuit.splatoon.util;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class AnnouncementHandler {
	private static final String BASE_MESSAGE = ChatColor.AQUA + "[Splatoon] " + ChatColor.GOLD + "%1$s";

	public static void announceArenaOnly(String message, Arena arena, Object... args) {
		if (!arena.getMatchHandler().hasMatch()) {
			return;
		}
		message = ChatColor.translateAlternateColorCodes('&', String.format(BASE_MESSAGE, String.format(
				Splatoon.getInstance().getLangConfig().getNode(message, String.class, "Missing lang element: " + message),
				args), message));
		for (Inkling inkling : arena.getMatchHandler().getCurrentMatch().getInklings()) {
			inkling.getPlayer().playNote(inkling.getPlayer().getLocation(), Instrument.PIANO, Note.natural(0, Note.Tone.F));
			inkling.getPlayer().sendMessage(message);
		}
	}

	public static void announceUserOnly(String message, CommandSender user, Object... args) {
		message = ChatColor.translateAlternateColorCodes('&', String.format(BASE_MESSAGE, String.format(
				Splatoon.getInstance().getLangConfig().getNode(message, String.class, "Missing lang element: " + message),
				args), message));
		user.sendMessage(message);
	}

	public static void announceExcludeArena(String message, Object... args) {
		message = ChatColor.translateAlternateColorCodes('&', String.format(BASE_MESSAGE, String.format(
				Splatoon.getInstance().getLangConfig().getNode(message, String.class, "Missing lang element: " + message),
				args), message));
		Bukkit.getServer().getConsoleSender().sendMessage(message);
		player_loop:
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			for (Arena arena : Splatoon.getInstance().getArenaManager().getArenas()) {
				if (arena.getMatchHandler().hasMatch() && arena.getMatchHandler().getCurrentMatch().getInkling(player.getUniqueId()) != null) {
					continue player_loop;
				}
			}
			player.sendMessage(message);
		}
	}

	public static void announce(String message, Object... args) {
		message = ChatColor.translateAlternateColorCodes('&', String.format(BASE_MESSAGE, String.format(
				Splatoon.getInstance().getLangConfig().getNode(message, String.class, "Missing lang element: " + message),
				args), message));
		Bukkit.getServer().getConsoleSender().sendMessage(message);
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			player.sendMessage(message);
		}
	}

	public static void announce(String message, String permission, Object... args) {
		message = ChatColor.translateAlternateColorCodes('&', String.format(BASE_MESSAGE, String.format(
				Splatoon.getInstance().getLangConfig().getNode(message, String.class, "Missing lang element: " + message),
				args), message));
		Bukkit.getServer().getConsoleSender().sendMessage(message);
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			if (PermissionUtils.hasPermission(player, permission)) {
				player.sendMessage(message);
			}
		}
	}
}
