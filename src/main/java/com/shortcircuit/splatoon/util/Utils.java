package com.shortcircuit.splatoon.util;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.player.TeamColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Utils {
	private static final ArrayList<Material> PAINTABLE_SURFACES = new ArrayList<>();
	private static final ArrayList<Material> COLORABLE_SURFACES = new ArrayList<>();
	private static final String[] DEFAULT_PAINTABLE_SURFACE = new String[]{"WOOL", "STAINED_CLAY", "STAINED_GLASS", "STAINED_GLASS_PANE"};

	public static boolean isInArena(Location location) {
		for (Arena arena : Splatoon.getInstance().getArenaManager().getArenas()) {
			if (isInArena(location, arena)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isInArena(Location location, Arena arena) {
		Vector pos = location.toVector();
		return location.getWorld().getUID().equals(arena.getWorldID())
				&& pos.isInAABB(arena.getMinPos().toVector(), arena.getMaxPos().add(1.0, 1.0, 1.0).toVector());
	}

	public static boolean isPlayerInMatch(Player player) {
		for (Arena arena : Splatoon.getInstance().getArenaManager().getArenas()) {
			if (!arena.getMatchHandler().hasMatch()) {
				continue;
			}
			if (arena.getMatchHandler().getCurrentMatch().getInkling(player.getUniqueId()) != null) {
				return true;
			}
		}
		return false;
	}

	public static boolean isPlayerInGame(Player player) {
		for (Arena arena : Splatoon.getInstance().getArenaManager().getArenas()) {
			if (!arena.getMatchHandler().isMatchStarted()) {
				continue;
			}
			if (arena.getMatchHandler().getCurrentMatch().getInkling(player.getUniqueId()) != null) {
				return true;
			}
		}
		return false;
	}

	public static ArrayList<Material> getPaintableSurfaces() {
		if (PAINTABLE_SURFACES.isEmpty()) {
			String[] paintable_surfaces = Splatoon.getInstance().getJsonConfig().getNode("paintable_surfaces", String[].class, DEFAULT_PAINTABLE_SURFACE);
			for (String material : paintable_surfaces) {
				PAINTABLE_SURFACES.add(Material.valueOf(material));
			}
		}
		return PAINTABLE_SURFACES;
	}

	public static boolean isColorableSurface(Material material) {
		return material == Material.CARPET
				|| material == Material.STAINED_CLAY
				|| material == Material.STAINED_GLASS
				|| material == Material.STAINED_GLASS_PANE
				|| material == Material.WOOL;
	}

	public static boolean isTeamColorData(byte data) {
		return TeamColor.getTeam(data) != null;
	}

	public static Color teamToColor(byte team_color) {
		DyeColor color = DyeColor.getByWoolData(team_color);
		return color == null ? DyeColor.WHITE.getFireworkColor() : color.getFireworkColor();
	}
}
