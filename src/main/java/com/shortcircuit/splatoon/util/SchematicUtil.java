package com.shortcircuit.splatoon.util;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import org.bukkit.Location;

import java.io.File;
import java.io.IOException;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SchematicUtil {
	private static final File ARENA_FILE = new File(Splatoon.getInstance().getDataFolder() + "/arenas");
	private static final int FAST_MODE_THRESHHOLD = 10000;
	private static final SchematicFormat SCHEMATIC_FORMAT = SchematicFormat.getFormat("mcedit");

	static {
		ARENA_FILE.mkdirs();
	}

	public static boolean regenArena(Arena arena) {
		try {
			Location origin = arena.getMinPos();
			File file = new File(ARENA_FILE + "/" + arena.getID() + ".schematic");
			//CuboidClipboard is deprecated in favor of Clipboard, however don't see a way to load a schematic into that class
			CuboidClipboard clipboard = SCHEMATIC_FORMAT.load(file);
			// Another interesting line of code becuase of the way getEditSession was deprecated
			EditSession edit_session = WorldEdit.getInstance().getEditSessionFactory().getEditSession(
					(com.sk89q.worldedit.world.World) BukkitUtil.getLocalWorld(origin.getWorld()), -1);
			if (clipboard.getLength() * clipboard.getWidth() * clipboard.getHeight() > FAST_MODE_THRESHHOLD) {
				edit_session.setFastMode(true);
			}
			clipboard.place(edit_session, BukkitUtil.toVector(origin), false);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean saveArena(Arena arena) {
		try {
			Vector min_pos = BukkitUtil.toVector(arena.getMinPos());
			Vector max_pos = BukkitUtil.toVector(arena.getMaxPos());
			Vector size = max_pos.subtract(min_pos);
			size = new Vector(Math.abs(size.getX()), Math.abs(size.getY()), Math.abs(size.getZ())).add(1, 1, 1);
			CuboidClipboard clipboard = new CuboidClipboard(size, BukkitUtil.toVector(arena.getMinPos()));
			// Another interesting line of code becuase of the way getEditSession was deprecated
			EditSession edit_session = WorldEdit.getInstance().getEditSessionFactory()
					.getEditSession((com.sk89q.worldedit.world.World) BukkitUtil.getLocalWorld(arena.getWorld()), -1);
			edit_session.setFastMode(true);
			clipboard.copy(edit_session);
			File file = new File(ARENA_FILE + "/" + arena.getID() + ".schematic");
			file.createNewFile();
			SCHEMATIC_FORMAT.save(clipboard, file);
			return true;
		}
		catch (IOException | DataException e) {
			e.printStackTrace();
		}
		return false;
	}
}
