package com.shortcircuit.utils.json;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 9/15/2015
 */
public class JsonUtils {
	private static final Gson gson = new GsonBuilder().serializeNulls().enableComplexMapKeySerialization()
			.setPrettyPrinting().disableHtmlEscaping().setExclusionStrategies(new ExclusionStrategy() {
				@Override
				public boolean shouldSkipField(FieldAttributes f) {
					return f.getAnnotation(Exclude.class) != null;
				}

				@Override
				public boolean shouldSkipClass(Class<?> clazz) {
					return clazz.getAnnotation(Exclude.class) != null;
				}
			}).create();

	/**
	 * @param obj The object to serialize
	 * @param <T> The Java type of the object
	 * @return The object serialized as a JsonElement
	 */
	public static <T> JsonElement toJson(T obj) {
		return gson.toJsonTree(obj, obj.getClass());
	}

	/**
	 * @param obj The object to serialize
	 * @param <T> The Java type of the object
	 * @return The object serialized as a formatted JSON string
	 */
	public static <T> String toJsonString(T obj) {
		return gson.toJson(toJson(obj));
	}

	/**
	 * @param json A valid JSON string representing an object
	 * @param type The Java type of the deserialized object
	 * @param <T>  The Java type of the deserialized object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(String json, TypeToken<T> type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type.getType());
	}

	/**
	 * @param json A valid JSON string representing an object
	 * @param type The Java type of the deserialized object
	 * @param <T>  The Java type of the deserialized object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(String json, Type type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type);
	}

	/**
	 * @param json A valid JSON string representing an object
	 * @param type The Java type of the object
	 * @param <T>  The Java type of the object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(String json, Class<T> type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type);
	}

	/**
	 * @param json A JsonElement representing an object
	 * @param type The Java type of the object
	 * @param <T>  The Java type of the object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(JsonElement json, TypeToken<T> type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type.getType());
	}

	/**
	 * @param json A JsonElement representing an object
	 * @param type The Java type of the object
	 * @param <T>  The Java type of the object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(JsonElement json, Type type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type);
	}

	/**
	 * @param json A JsonElement representing an object
	 * @param type The Java type of the object
	 * @param <T>  The Java type of the object
	 * @return The deserialized object
	 * @throws JsonSyntaxException
	 * @throws ExceptionInInitializerError
	 */
	public static <T> T fromJson(JsonElement json, Class<T> type) throws JsonSyntaxException, ExceptionInInitializerError {
		return gson.fromJson(json, type);
	}
}
