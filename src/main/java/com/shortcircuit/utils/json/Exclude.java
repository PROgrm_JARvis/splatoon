package com.shortcircuit.utils.json;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * Fields and classes annotated with this are ignored when using
 * {@link com.shortcircuit.utils.json.JsonUtils JsonUtils} methods
 *
 * @author ShortCircuit908
 *         Created on 9/15/2015
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Exclude {

}
