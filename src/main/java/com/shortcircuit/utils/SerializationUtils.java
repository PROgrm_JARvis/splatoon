package com.shortcircuit.utils;

import java.io.*;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/7/2015
 */
public class SerializationUtils {
	/**
	 * Serialize an object into a <code>byte[]</code> array
	 * <p/>
	 * The object must implement {@link Serializable Serializable} as specified in
	 * {@link ObjectOutputStream#writeObject(Object)}
	 *
	 * @param object The object to serialize
	 * @return An array of bytes representing the object
	 * @throws IOException
	 */
	public static byte[] serializeObject(Object object) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream obj_out = new ObjectOutputStream(out);
		obj_out.writeObject(object);
		obj_out.flush();
		obj_out.close();
		return out.toByteArray();
	}

	/**
	 * Deserialize a <code>byte[]</code> array into an object
	 *
	 * @param data An array of bytes representing an object
	 * @return The deserialized object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object deserializeObject(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream obj_in = new ObjectInputStream(in);
		return obj_in.readObject();
	}
}
