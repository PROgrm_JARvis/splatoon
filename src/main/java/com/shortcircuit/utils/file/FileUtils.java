package com.shortcircuit.utils.file;

import com.shortcircuit.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/29/2015
 */
public class FileUtils {
	public static void copyAll(File source, File dest) throws IOException {
		if (source.isDirectory()) {
			dest.mkdirs();
			for (File file : source.listFiles()) {
				copyAll(file, new File(dest + "/" + file.getName()));
			}
			return;
		}
		if (!dest.exists()) {
			dest.getParentFile().mkdirs();
			dest.createNewFile();
		}
		FileInputStream in = new FileInputStream(source);
		FileOutputStream out = new FileOutputStream(dest);
		Utils.copyStream(in, out);
		out.close();
	}

	public static void deleteAll(File file) throws SecurityException {
		if (file.isDirectory()) {
			for (File sub : file.listFiles()) {
				deleteAll(sub);
			}
		}
		file.delete();
	}

	public static long getTotalSize(File file) {
		long size = 0;
		if (file.isDirectory()) {
			for (File sub : file.listFiles()) {
				size += getTotalSize(sub);
			}
		}
		else {
			size += file.length();
		}
		return size;
	}
}
