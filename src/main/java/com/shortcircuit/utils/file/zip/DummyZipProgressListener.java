package com.shortcircuit.utils.file.zip;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/29/2015
 */
class DummyZipProgressListener extends ZipProgressListener {

	protected DummyZipProgressListener() {

	}

	@Override
	public void updateTotalLength(long length) {

	}

	@Override
	public void updateTotalProgress(long progress) {

	}

	@Override
	public void updateFileLength(long file_length) {

	}

	@Override
	public void updateFileProgress(long file_progress) {

	}

	@Override
	public void updateProcessedFile(String file) {

	}
}
