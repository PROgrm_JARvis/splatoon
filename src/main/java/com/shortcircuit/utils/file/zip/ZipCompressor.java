package com.shortcircuit.utils.file.zip;

import com.shortcircuit.utils.Utils;
import com.shortcircuit.utils.file.FileUtils;

import java.io.*;
import java.util.zip.*;


/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/28/2015
 */
public class ZipCompressor {

	public static void packZlib(File source_file, File output_file) throws IOException{
		packZlib(new FileInputStream(source_file), new FileOutputStream(output_file));
	}


	public static void packZlib(InputStream source, OutputStream dest) throws IOException{
		DeflaterOutputStream out = new DeflaterOutputStream(dest);
		Utils.copyStream(source, dest);
		out.flush();
		out.close();
	}

	public static void packGZIP(File source_file, File output_file) throws IOException {
		packGZIP(new FileInputStream(source_file), new FileOutputStream(output_file));
	}

	public static void packGZIP(InputStream source, OutputStream dest) throws IOException {
		GZIPOutputStream out = new GZIPOutputStream(dest);
		Utils.copyStream(source, out);
		out.flush();
		out.close();
	}

	public static void packZip(File[] source_files, File output_file, int compression_method, ZipProgressListener progress_listener) throws IOException {
		if (progress_listener == null) {
			progress_listener = new DummyZipProgressListener();
		}
		ZipOutputStream zip_out = new ZipOutputStream(new FileOutputStream(output_file));
		zip_out.setLevel(compression_method);
		long total_size = 0;
		for (File file : source_files) {
			total_size += FileUtils.getTotalSize(file);
		}
		progress_listener.updateTotalProgress(0);
		progress_listener.updateTotalLength(total_size);
		progress_listener.updateFileProgress(0);
		progress_listener.updateFileLength(0);
		progress_listener.updateProcessedFile(null);
		long total_progress = 0;
		for (File source : source_files) {
			if (source.isDirectory()) {
				total_progress += zipDir(zip_out, "", source, progress_listener, total_progress);
			}
			else {
				total_progress += zipFile(zip_out, "", source, progress_listener, total_progress);
			}
		}
		zip_out.flush();
		zip_out.close();
	}

	public static void packZip(File[] source_files, File output_file, int compression_method) throws IOException {
		packZip(source_files, output_file, compression_method, null);
	}

	private static String buildPath(String path, String file) {
		if (path == null || (path = path.trim()).isEmpty()) {
			return file.trim();
		}
		return path + "\\" + file.trim();
	}

	private static long zipDir(ZipOutputStream zos, String path, File dir, ZipProgressListener progress_listener, long total_progress) throws IOException {
		progress_listener.updateProcessedFile(dir.getAbsolutePath());
		progress_listener.updateFileLength(0);
		progress_listener.updateFileProgress(0);
		if (!dir.canRead()) {
			return total_progress;
		}
		File[] files = dir.listFiles();
		path = buildPath(path, dir.getName());
		for (File source : files) {
			if (source.isDirectory()) {
				total_progress += zipDir(zos, path, source, progress_listener, total_progress);
			}
			else {
				total_progress += zipFile(zos, path, source, progress_listener, total_progress);
			}
		}
		return total_progress;
	}

	private static long zipFile(ZipOutputStream zos, String path, File file, ZipProgressListener progress_listener, long total_progress) throws IOException {
		progress_listener.updateProcessedFile(file.getAbsolutePath());
		progress_listener.updateFileProgress(0);
		progress_listener.updateFileLength(file.length());
		if (!file.canRead()) {
			return total_progress;
		}
		zos.putNextEntry(new ZipEntry(buildPath(path, file.getName())));
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[1024];
		long total_count = 0;
		int byte_count;
		while ((byte_count = fis.read(buffer)) != -1) {
			zos.write(buffer, 0, byte_count);
			progress_listener.updateFileProgress(total_count += byte_count);
			progress_listener.updateTotalProgress(total_progress += byte_count);
		}
		fis.close();
		zos.closeEntry();
		return total_progress;
	}
}