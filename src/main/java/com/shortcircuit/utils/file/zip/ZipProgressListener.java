package com.shortcircuit.utils.file.zip;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/29/2015
 */
public abstract class ZipProgressListener {
	public abstract void updateTotalLength(long length);

	public abstract void updateTotalProgress(long progress);

	public abstract void updateFileLength(long file_length);

	public abstract void updateFileProgress(long file_progress);

	public abstract void updateProcessedFile(String file);
}
