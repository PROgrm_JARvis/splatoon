package com.shortcircuit.utils.file.zip;

import com.shortcircuit.utils.Utils;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/28/2015
 */
public class ZipExtractor {

	public static void extractZlib(File source_file, File output_file) throws IOException {
		output_file.createNewFile();
		FileOutputStream out = new FileOutputStream(output_file);
		InputStream in = extractGZIP(source_file);
		Utils.copyStream(in, out);
		out.flush();
		out.close();
	}

	public static InputStream extractZlib(File source_file) throws IOException {
		return extractZlib(new FileInputStream(source_file));
	}

	public static InputStream extractZlib(InputStream source) throws IOException {
		return new InflaterInputStream(source);
	}

	public static void extractGZIP(File source_file, File output_file) throws IOException {
		output_file.createNewFile();
		FileOutputStream out = new FileOutputStream(output_file);
		InputStream in = extractGZIP(source_file);
		Utils.copyStream(in, out);
		out.flush();
		out.close();
	}

	public static InputStream extractGZIP(File source_file) throws IOException {
		return extractGZIP(new FileInputStream(source_file));
	}

	public static InputStream extractGZIP(InputStream source) throws IOException {
		return new GZIPInputStream(source);
	}

	public static void extractZIP(File zip, File dest) throws IOException {
		extractZIP(zip, dest, null);
	}

	public static void extractZIP(File zip, File dest, ZipProgressListener progress_listener) throws IOException {
		if (progress_listener == null) {
			progress_listener = new DummyZipProgressListener();
		}
		byte[] buffer = new byte[1024];
		ZipFile zip_file = new ZipFile(zip);
		Enumeration<? extends ZipEntry> entries = zip_file.entries();
		long total_length = 0;
		while (entries.hasMoreElements()) {
			total_length += entries.nextElement().getSize();
		}
		progress_listener.updateTotalProgress(0);
		progress_listener.updateTotalLength(total_length);
		progress_listener.updateFileProgress(0);
		progress_listener.updateFileLength(0);
		progress_listener.updateProcessedFile(null);
		entries = zip_file.entries();
		long total_progress = 0;
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			String file_name = entry.getName();
			progress_listener.updateFileProgress(0);
			progress_listener.updateFileLength(0);
			progress_listener.updateProcessedFile(file_name);
			File file = new File(dest + "\\" + file_name);
			new File(file.getParent()).mkdirs();
			if (entry.isDirectory()) {
				file.mkdir();
			}
			else {
				progress_listener.updateFileLength(entry.getSize());
				FileOutputStream out = new FileOutputStream(file);
				long total_written = 0;
				int len;
				InputStream in = zip_file.getInputStream(entry);
				while ((len = in.read(buffer)) != -1) {
					out.write(buffer, 0, len);
					progress_listener.updateFileProgress(total_written += len);
					progress_listener.updateTotalProgress(total_progress += len);
				}
				out.flush();
				out.close();
				in.close();
			}
		}
	}
}