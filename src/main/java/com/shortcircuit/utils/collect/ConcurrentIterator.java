package com.shortcircuit.utils.collect;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/3/2015
 */
public class ConcurrentIterator<E> implements Iterator<E> {
	private final ConcurrentArrayList<E> list;
	private int last_index = -1;
	private int index = 0;
	private int size = 0;

	protected ConcurrentIterator(ConcurrentArrayList<E> list) {
		this.list = list;
		size = list.size();
	}

	@Override
	public synchronized boolean hasNext() {
		synchronized (this) {
			return index < size;
		}
	}

	@Override
	public synchronized E next() {
		synchronized (this) {
			if (index >= size) {
				throw new NoSuchElementException();
			}
			last_index = index;
			return list.get(index++);
		}
	}

	@Override
	public synchronized void remove() {
		synchronized (this) {
			if (last_index < 0) {
				throw new IllegalStateException();
			}
			list.remove(last_index);
			index = last_index;
			size = list.size();
		}
	}
}
