package com.shortcircuit.utils.collect;

import java.util.*;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * Thread-safe wrapper for a {@link ArrayList ArrayList} which guards against
 * {@link ConcurrentModificationException ConcurrentModificationException}s
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
@SuppressWarnings("NullableProblems")
public class ConcurrentArrayList<T> extends ArrayList<T> {

	public ConcurrentArrayList() {
		super();
	}

	public ConcurrentArrayList(int initial_capacity) {
		super(initial_capacity);
	}

	public ConcurrentArrayList(Collection<? extends T> c) {
		super(c);
	}

	public ConcurrentArrayList(T[] c) {
		super(c.length);
		for (T element : c) {
			add(element);
		}
	}

	@Override
	public synchronized int size() {
		synchronized (this) {
			return super.size();
		}
	}

	@Override
	public synchronized boolean isEmpty() {
		synchronized (this) {
			return super.isEmpty();
		}
	}

	@Override
	public synchronized boolean contains(Object o) {
		synchronized (this) {
			return super.contains(o);
		}
	}

	@Override
	public synchronized ConcurrentIterator<T> iterator() {
		synchronized (this) {
			return new ConcurrentIterator<>(this);
		}
	}

	@Override
	public synchronized Object[] toArray() {
		synchronized (this) {
			return super.toArray();
		}
	}

	@Override
	public synchronized <T1> T1[] toArray(T1[] a) {
		synchronized (this) {
			return super.toArray(a);
		}
	}

	@Override
	public synchronized boolean add(T t) {
		synchronized (this) {
			return super.add(t);
		}
	}

	@Override
	public synchronized boolean remove(Object o) {
		synchronized (this) {
			return super.remove(o);
		}
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		synchronized (this) {
			return super.containsAll(c);
		}
	}

	@Override
	public synchronized boolean addAll(Collection<? extends T> c) {
		synchronized (this) {
			return super.addAll(c);
		}
	}

	@Override
	public synchronized boolean addAll(int index, Collection<? extends T> c) {
		synchronized (this) {
			return super.addAll(index, c);
		}
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		synchronized (this) {
			return super.removeAll(c);
		}
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		synchronized (this) {
			return super.retainAll(c);
		}
	}

	@Override
	public synchronized void clear() {
		synchronized (this) {
			super.clear();
		}
	}

	@Override
	public synchronized T get(int index) {
		synchronized (this) {
			return super.get(index);
		}
	}

	@Override
	public synchronized T set(int index, T element) {
		synchronized (this) {
			return super.set(index, element);
		}
	}

	@Override
	public synchronized void add(int index, T element) {
		synchronized (this) {
			super.add(index, element);
		}
	}

	@Override
	public synchronized T remove(int index) {
		synchronized (this) {
			return super.remove(index);
		}
	}

	@Override
	public synchronized int indexOf(Object o) {
		synchronized (this) {
			return super.indexOf(o);
		}
	}

	@Override
	public synchronized int lastIndexOf(Object o) {
		synchronized (this) {
			return super.lastIndexOf(o);
		}
	}

	@Override
	public synchronized ConcurrentListIterator<T> listIterator() {
		synchronized (this) {
			return new ConcurrentListIterator<>(this, 0);
		}
	}

	@Override
	public synchronized ConcurrentListIterator<T> listIterator(int index) {
		synchronized (this) {
			return new ConcurrentListIterator<>(this, index);
		}
	}

	@Override
	public synchronized ConcurrentArrayList<T> subList(int from_index, int to_index) {
		synchronized (this) {
			return new ConcurrentArrayList<>(super.subList(from_index, to_index));
		}
	}

	@Override
	public synchronized void ensureCapacity(int min_capacity) {
		synchronized (this) {
			super.ensureCapacity(min_capacity);
		}
	}

	@Override
	public synchronized void trimToSize() {
		synchronized (this) {
			super.trimToSize();
		}
	}

	@Override
	public synchronized Object clone() {
		synchronized (this) {
			return super.clone();
		}
	}
}
