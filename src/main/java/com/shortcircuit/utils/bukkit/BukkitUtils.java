package com.shortcircuit.utils.bukkit;

import com.shortcircuit.utils.Version;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;

import java.util.HashSet;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 9/29/2015
 */
public class BukkitUtils {
	private static final HashSet<Material> TRANSPARENT_BLOCKS = getTransparentBlocks();
	private static final HashSet<Byte> LEGACY_TRANSPARENT_BLOCKS = getLegacyTransparentBlocks();
	private static final Boolean title_supported = isTitleSupported();

	/**
	 * Check if the server's version of CraftBukkit supports titles
	 *
	 * @return <code>true</code> if titles are supported
	 */
	public static boolean isTitleSupported() {
		if (title_supported == null) {
			return new Version(Bukkit.getBukkitVersion().split("\\-")[0]).compareTo(new Version(1, 8, 8)) >= 0;
		}
		return title_supported;
	}

	public static HashSet<Byte> getLegacyTransparentBlocks() {
		if(LEGACY_TRANSPARENT_BLOCKS == null) {
			HashSet<Byte> legacy_transparent_blocks = new HashSet<>();
			HashSet<Material> transparent_blocks = getTransparentBlocks();
			for (Material material : transparent_blocks) {
				legacy_transparent_blocks.add((byte) material.getId());
			}
			return legacy_transparent_blocks;
		}
		return LEGACY_TRANSPARENT_BLOCKS;
	}

	public static HashSet<Material> getTransparentBlocks() {
		if (TRANSPARENT_BLOCKS == null) {
			HashSet<Material> blocks = new HashSet<>();
			for (Material material : Material.values()) {
				if (material.isTransparent()) {
					blocks.add(material);
				}
			}
			return blocks;
		}
		return TRANSPARENT_BLOCKS;
	}

	public static void addTransparentBlock(String material_name){
		Material material = Material.valueOf(material_name);
		TRANSPARENT_BLOCKS.add(material);
		LEGACY_TRANSPARENT_BLOCKS.add((byte) material.getId());
	}

	public static Location getNeighboringBlock(Location origin, BlockFace... faces) {
		int mod_x = 0;
		int mod_y = 0;
		int mod_z = 0;
		for (BlockFace face : faces) {
			mod_x += face.getModX();
			mod_y += face.getModY();
			mod_z += face.getModZ();
		}
		return origin.add(mod_x, mod_y, mod_z);
	}
}
