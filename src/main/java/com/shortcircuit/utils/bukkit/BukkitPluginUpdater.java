package com.shortcircuit.utils.bukkit;

import com.google.gson.annotations.SerializedName;
import com.shortcircuit.utils.Version;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import com.shortcircuit.utils.file.zip.ZipExtractor;
import com.shortcircuit.utils.json.JsonUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.util.Scanner;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/28/2015
 */
public class BukkitPluginUpdater implements Runnable, Listener {
	private final Plugin plugin;
	private final Integer plugin_id;
	private final String slug;
	private final String api_key;
	private final boolean download;
	private Result result = null;

	private BukkitPluginUpdater(Plugin plugin, Integer plugin_id, String slug, String api_key, boolean download) {
		this.plugin = plugin;
		this.plugin_id = plugin_id;
		this.slug = slug;
		this.api_key = api_key;
		this.download = download;
		plugin.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, this, 0);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin   The plugin to update
	 * @param slug     The plugin's slug on dev.bukkit.org
	 * @param download If <code>true</code>, any updates will be automatically downloaded
	 */
	public BukkitPluginUpdater(Plugin plugin, String slug, boolean download) {
		this(plugin, null, slug, null, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin   The plugin to update
	 * @param slug     The plugin's slug on dev.bukkit.org
	 * @param api_key  The developer's API key
	 * @param download If <code>true</code>, any updates will be automatically downloaded
	 */
	public BukkitPluginUpdater(Plugin plugin, String slug, String api_key, boolean download) {
		this(plugin, null, slug, api_key, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param plugin_id The plugin's ID on dev.bukkit.org
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public BukkitPluginUpdater(Plugin plugin, int plugin_id, boolean download) {
		this(plugin, plugin_id, null, null, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param plugin_id The plugin's ID on dev.bukkit.org
	 * @param api_key   The developer's API key
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public BukkitPluginUpdater(Plugin plugin, int plugin_id, String api_key, boolean download) {
		this(plugin, plugin_id, null, api_key, download);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void notify(final PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (result != null && PermissionUtils.hasPermission(player, "updater.notify")) {
			switch (result.state) {
				case INFO_FETCH_FAILED:
				case FAILED:
				case KEY_REJECTED:
				case INVALID_FORMAT:
					player.sendMessage(ChatColor.RED + String.format("[%1$s] Failed to check Bukkit for updates. Please check " +
							"the console for more information.", plugin.getName()));
					if (result.exception != null) {
						player.sendMessage(ChatColor.RED + String.format("[%1$s] %2$s: %3$s", plugin.getName(),
								result.exception.getClass().getCanonicalName(), result.exception.getMessage()));
					}
					break;
				case UPDATE_DOWNLOADED:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s %3$s for %4$s",
							plugin.getName(), result.latest_version_info.release_type, result.latest_version_info.name,
							result.latest_version_info.game_version));
					switch (result.download_result.state) {
						case DOWNLOAD_SUCCESS:
							player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Please restart the server to apply the update",
									plugin.getName()));
							break;
						case DOWNLOAD_FAIL:
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Failed to download update. " +
									"Please check the console for more information.", plugin.getName()));
							player.sendMessage(ChatColor.RED + String.format("[%1$s] %2$s: %3$s", plugin.getName(),
									result.download_result.exception.getClass().getCanonicalName(),
									result.download_result.exception.getMessage()));
							break;
						case BAD_CHECKSUM:
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Downloaded update is corrupted",
									plugin.getName()));
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Please download the file manually at %2$s",
									plugin.getName(), result.latest_version_info.download_url));
							break;
					}
					break;
				case NOT_UPDATED:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] The plugin has not been updated", plugin.getName()));
					break;
				case UPDATE_AVAILABLE:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s %3$s for %4$s",
							plugin.getName(), result.latest_version_info.release_type, result.latest_version_info.name,
							result.latest_version_info.game_version));
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Download the update at %2$s", plugin.getName(),
							result.latest_version_info.download_url));
					break;
				case UP_TO_DATE:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Plugin is up-to-date", plugin.getName()));
					break;
				case NEWER_VERSION:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Plugin version is newer than released updates", plugin.getName()));
					break;
			}
		}
	}

	@Override
	public void run() {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		if (plugin_id != null) {
			result = query(plugin, plugin_id, api_key, download);
		}
		else {
			result = query(plugin, slug, api_key, download);
		}
	}

	/**
	 * Check for updates on dev.bukkit.org
	 *
	 * @param plugin   The plugin to update
	 * @param slug     The plugin's slug on dev.bukkit.org
	 * @param download If <code>true</code>, any updates will be automatically downloaded
	 * @return The result of the query
	 */
	public static Result query(Plugin plugin, String slug, boolean download) {
		return query(plugin, slug, null, download);
	}

	/**
	 * Check for updates on dev.bukkit.org
	 *
	 * @param plugin   The plugin to update
	 * @param slug     The plugin's slug on dev.bukkit.org
	 * @param api_key  The developer's API key
	 * @param download If <code>true</code>, any updates will be automatically downloaded
	 * @return The result of the query
	 */
	public static Result query(Plugin plugin, String slug, String api_key, boolean download) {
		try {
			PluginInfo info = getPluginInfo(slug);
			if (info == null) {
				throw new IOException("Unable to retrieve plugin info");
			}
			return query(plugin, info.id, api_key, download);
		}
		catch (IOException e) {
			return new Result(ResultState.INFO_FETCH_FAILED, e);
		}
	}

	/**
	 * Check for updates on dev.bukkit.org
	 *
	 * @param plugin    The plugin to update
	 * @param plugin_id The plugin's ID on dev.bukkit.org
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 * @return The result of the query
	 */
	public static Result query(Plugin plugin, int plugin_id, boolean download) {
		return query(plugin, plugin_id, null, download);
	}

	/**
	 * Check for updates on dev.bukkit.org
	 *
	 * @param plugin    The plugin to update
	 * @param plugin_id The plugin's ID on dev.bukkit.org
	 * @param api_key   The developer's API key
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 * @return The result of the query
	 */
	public static Result query(Plugin plugin, int plugin_id, String api_key, boolean download) {
		try {
			plugin.getLogger().info("Checking Bukkit for updates...");
			HttpURLConnection connection =
					(HttpURLConnection) new URL("https://api.curseforge.com/servermods/files?projectIds=" + plugin_id).openConnection();
			connection.setConnectTimeout(5000);
			if (api_key != null) {
				connection.addRequestProperty("X-API-Key", api_key);
			}
			connection.addRequestProperty("User-Agent", "BukkitPluginUpdater (by ShortCircuit908)");
			switch (connection.getResponseCode()) {
				case 403: // API key rejected
					plugin.getLogger().warning("The server rejected the provided API key");
					plugin.getLogger().warning("Please contact the plugin author(s) to fix this error");
					return new Result(ResultState.KEY_REJECTED);
			}
			StringBuilder response_builder = new StringBuilder();
			Scanner scanner = new Scanner(connection.getInputStream());
			while (scanner.hasNextLine()) {
				response_builder.append(scanner.nextLine());
			}
			UpdateInfo[] update_list = JsonUtils.fromJson(response_builder.toString().replace("\\/", "/"), UpdateInfo[].class);
			Version plugin_version = new Version(plugin.getDescription().getVersion());
			UpdateInfo latest_version_info = null;
			Version latest_version = null;
			for (UpdateInfo update_info : update_list) {
				if (!update_info.name.toLowerCase().matches("(\\w+\\s)*v\\d(\\.\\d)*(-snapshot)?")) {
					plugin.getLogger().warning("The author of this plugin has misconfigured their update system");
					plugin.getLogger().warning("Version names should follow the format \"{plugin_name} v{version}\"");
					plugin.getLogger().warning("Please contact the plugin author(s) to fix this error");
					return new Result(ResultState.INVALID_FORMAT);
				}
				String raw_version = update_info.name.toLowerCase().replaceFirst("(\\w+\\s)*v", "");
				Version version = new Version(raw_version);
				if (version.compareTo(latest_version) > 0) {
					latest_version = version;
					latest_version_info = update_info;
				}
			}
			if (latest_version == null) {
				plugin.getLogger().info("This plugin has not been updated");
				return new Result(ResultState.NOT_UPDATED);
			}
			int compare = latest_version.compareTo(plugin_version);
			if (compare < 0) {
				plugin.getLogger().info("This plugin's version is newer than any public releases");
				plugin.getLogger().info("Are you a developer, or a time traveler?");
				return new Result(ResultState.NEWER_VERSION);
			}
			else if (compare > 0) {
				plugin.getLogger().info("An update is available: " + latest_version_info.release_type + " "
						+ latest_version_info.name + " for " + latest_version_info.game_version);
				if (download) {
					DownloadResult result = downloadUpdate(plugin, latest_version_info.file_name,
							latest_version_info.download_url, latest_version_info.md5);
					return new Result(ResultState.UPDATE_DOWNLOADED, latest_version, latest_version_info, result);
				}
				else {
					plugin.getLogger().info("Download the update at " + latest_version_info.download_url);
				}
				return new Result(ResultState.UPDATE_AVAILABLE, latest_version, latest_version_info);
			}
			else {
				plugin.getLogger().info("This plugin is up-to-date");
				return new Result(ResultState.UP_TO_DATE);
			}
		}
		catch (IOException e) {
			return new Result(ResultState.FAILED, e);
		}
	}

	private static DownloadResult downloadUpdate(Plugin plugin, String filename, URL url, String checksum) {
		try {
			File output_dir = plugin.getServer().getUpdateFolderFile();
			plugin.getLogger().info("Downloading update...");
			output_dir.mkdirs();
			File download_file = new File(output_dir + "/" + filename);
			download_file.createNewFile();
			Files.copy(url.openStream(), download_file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			plugin.getLogger().info("Validating download...");
			if (!validateFile(download_file, checksum)) {
				plugin.getLogger().warning("Downloaded file is not valid");
				plugin.getLogger().warning("Please download the file manually at " + url);
				download_file.delete();
				return new DownloadResult(DownloadResultState.BAD_CHECKSUM);
			}
			if (download_file.getName().toLowerCase().endsWith(".zip")) {
				plugin.getLogger().info("Extracting files...");
				ZipExtractor.extractZIP(download_file, output_dir);
			}
			plugin.getLogger().info("Update downloaded");
			plugin.getLogger().info("Please restart the server to apply the update");
		}
		catch (IOException e) {
			return new DownloadResult(DownloadResultState.DOWNLOAD_FAIL, e);
		}
		return new DownloadResult(DownloadResultState.DOWNLOAD_SUCCESS);
	}

	/**
	 * Query a plugin's info given it's slug
	 *
	 * @param slug The plugin's slug
	 * @return The plugin's info
	 * @throws IOException
	 */
	public static PluginInfo getPluginInfo(String slug) throws IOException {
		HttpURLConnection connection =
				(HttpURLConnection) new URL("https://api.curseforge.com/servermods/projects?search=" + slug).openConnection();
		connection.setConnectTimeout(5000);
		StringBuilder response_builder = new StringBuilder();
		Scanner scanner = new Scanner(connection.getInputStream());
		while (scanner.hasNextLine()) {
			response_builder.append(scanner.nextLine());
		}
		PluginInfo[] plugins = JsonUtils.fromJson(response_builder.toString(), PluginInfo[].class);
		for (PluginInfo info : plugins) {
			if (info.slug.equals(slug)) {
				return info;
			}
		}
		return null;
	}

	public enum ReleaseType {
		@SerializedName("alpha")
		ALPHA,
		@SerializedName("beta")
		BETA,
		@SerializedName("release")
		RELEASE
	}

	public class PluginInfo {
		private int id;
		private String name;
		private String slug;
		private ReleaseType stage;

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getSlug() {
			return slug;
		}

		public ReleaseType getStage() {
			return stage;
		}
	}

	private static boolean validateFile(File file, String checksum) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			FileInputStream in = new FileInputStream(file);
			int read;
			byte[] buffer = new byte[1024];
			while ((read = in.read(buffer)) != -1) {
				md.update(buffer, 0, read);
			}
			byte[] digest = md.digest();
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < digest.length; i++) {
				builder.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}
			return checksum.equalsIgnoreCase(builder.toString());
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static class UpdateInfo {
		@SerializedName("downloadUrl")
		private URL download_url;
		@SerializedName("fileName")
		private String file_name;
		@SerializedName("fileUrl")
		private URL file_url;
		@SerializedName("gameVersion")
		private String game_version;
		private String md5;
		private String name;
		@SerializedName("projectId")
		private int project_id;
		@SerializedName("releaseType")
		private ReleaseType release_type;

		protected UpdateInfo(URL download_url, String file_name, int project_id) {
			this.download_url = download_url;
			this.file_name = file_name;
			this.project_id = project_id;
		}

		public URL getDownloadUrl() {
			return download_url;
		}

		public String getFileName() {
			return file_name;
		}

		public URL getFileUrl() {
			return file_url;
		}

		public String getGameVersion() {
			return game_version;
		}

		public String getMd5Hash() {
			return md5;
		}

		public String getName() {
			return name;
		}

		public int getProjectId() {
			return project_id;
		}

		public ReleaseType getReleaseType() {
			return release_type;
		}
	}

	public static class Result {
		private final ResultState state;
		private final Exception exception;
		private final Version latest_version;
		private final UpdateInfo latest_version_info;
		private final DownloadResult download_result;

		protected Result(ResultState state, Version latest_version, UpdateInfo latest_version_info, DownloadResult download_result) {
			this.state = state;
			this.latest_version = latest_version;
			this.latest_version_info = latest_version_info;
			this.exception = null;
			this.download_result = download_result;
		}


		protected Result(ResultState state, Version latest_version, UpdateInfo latest_version_info) {
			this(state, latest_version, latest_version_info, null);
		}

		protected Result(ResultState state, Exception exception) {
			this.state = state;
			this.latest_version = null;
			this.latest_version_info = null;
			this.exception = exception;
			this.download_result = null;
		}

		protected Result(ResultState state) {
			this(state, null);
		}

		public ResultState getState() {
			return state;
		}

		public Exception getException() {
			return exception;
		}

		public Version getLatestVersion() {
			return latest_version;
		}

		public UpdateInfo getLatestVersionInfo() {
			return latest_version_info;
		}

		public DownloadResult getDownloadResult() {
			return download_result;
		}
	}

	public enum ResultState {
		INFO_FETCH_FAILED,
		FAILED,
		UPDATE_DOWNLOADED,
		KEY_REJECTED,
		INVALID_FORMAT,
		NOT_UPDATED,
		UPDATE_AVAILABLE,
		UP_TO_DATE,
		NEWER_VERSION
	}

	public enum DownloadResultState {
		DOWNLOAD_SUCCESS,
		DOWNLOAD_FAIL,
		BAD_CHECKSUM
	}

	public static class DownloadResult {
		private final DownloadResultState state;
		private final boolean success;
		private final Exception exception;

		private DownloadResult(DownloadResultState state) {
			this.state = state;
			this.success = true;
			this.exception = null;
		}

		private DownloadResult(DownloadResultState state, Exception exception) {
			this.state = state;
			this.success = false;
			this.exception = exception;
		}

		public DownloadResultState getState() {
			return state;
		}

		public boolean successful() {
			return success;
		}

		public Exception getException() {
			return exception;
		}
	}
}
