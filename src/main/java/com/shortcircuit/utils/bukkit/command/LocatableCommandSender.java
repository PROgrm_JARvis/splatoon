package com.shortcircuit.utils.bukkit.command;

import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * Wrapper for a {@link CommandSender CommandSender} which has a {@link Location Location} associated with it
 * <p/>
 * Applies to {@link BlockCommandSender BlockCommandSender} and {@link Player Player}
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
public class LocatableCommandSender extends CommandSenderWrapper<CommandSender> {

	protected LocatableCommandSender(CommandSender wrap) {
		super(wrap);
		assert wrap instanceof BlockCommandSender || wrap instanceof Player;
	}

	/**
	 * Get the {@link Location Location} associated with the sender
	 *
	 * @return The sender's location
	 */
	public Location getLocation() {
		if (wrap instanceof BlockCommandSender) {
			return ((BlockCommandSender) wrap).getBlock().getLocation();
		}
		return ((Player) wrap).getLocation();
	}

	/**
	 * Check if the command sender is a {@link Player Player}
	 *
	 * @return <code>true</code> if the sender is a {@link Player Player}
	 */
	public boolean isPlayer() {
		return wrap instanceof Player;
	}

	/**
	 * Check if the command sender is a {@link BlockCommandSender BlockCommandSender}
	 *
	 * @return <code>true</code> if the sender is a {@link BlockCommandSender BlockCommandSender}
	 */
	public boolean isBlock() {
		return wrap instanceof BlockCommandSender;
	}

	/**
	 * Cast the wrapped sender to a {@link Player Player}
	 *
	 * @return The sender as a {@link Player Player}
	 * @throws ClassCastException
	 */
	public Player asPlayer() throws ClassCastException {
		return (Player) wrap;
	}

	/**
	 * Cast the wrapped sender to a {@link BlockCommandSender BlockCommandSender}
	 *
	 * @return The sender as a {@link BlockCommandSender BlockCommandSender}
	 * @throws ClassCastException
	 */
	public BlockCommandSender asBlock() throws ClassCastException {
		return (BlockCommandSender) wrap;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof LocatableCommandSender && wrap.equals(((CommandSenderWrapper) other).wrap);
	}
}