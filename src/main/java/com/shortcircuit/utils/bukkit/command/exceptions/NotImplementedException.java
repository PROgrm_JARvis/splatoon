package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/10/2015
 */
public class NotImplementedException extends CommandException {
	public NotImplementedException() {
		super("This command has not been implemented.");
	}
}
