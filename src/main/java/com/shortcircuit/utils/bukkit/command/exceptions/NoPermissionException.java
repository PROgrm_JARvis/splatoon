package com.shortcircuit.utils.bukkit.command.exceptions;


/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class NoPermissionException extends CommandException {
	private final String required_permissions;

	public NoPermissionException(){
		this(null);
	}

	public NoPermissionException(String required_permissions) {
		super("I'm sorry, but you do not have permission to perform this command. Please contact " +
				"the server administrators if you believe this is in error.");
		this.required_permissions = required_permissions;
	}

	/**
	 * Gets the missing required permissions
	 *
	 * @return The missing permissions
	 */
	public String getRequiredPermissions() {
		return required_permissions;
	}
}
