package com.shortcircuit.utils.bukkit.command;

import org.bukkit.Server;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import java.util.Set;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
class CommandSenderWrapper<T extends CommandSender> implements CommandSender {
	protected final T wrap;

	protected CommandSenderWrapper(T wrap) {
		assert wrap instanceof BlockCommandSender || wrap instanceof Player;
		this.wrap = wrap;
	}

	public T getWrap() {
		return wrap;
	}

	@Override
	public void sendMessage(String message) {
		wrap.sendMessage(message);
	}

	public void sendMessage(String[] messages) {
		wrap.sendMessage(messages);
	}

	@Override
	public Server getServer() {
		return wrap.getServer();
	}

	@Override
	public String getName() {
		return wrap.getName();
	}

	@Override
	public boolean isPermissionSet(String name) {
		return wrap.isPermissionSet(name);
	}

	@Override
	public boolean isPermissionSet(Permission perm) {
		return wrap.isPermissionSet(perm);
	}

	@Override
	public boolean hasPermission(String name) {
		return PermissionUtils.hasPermission(wrap, name);
	}

	@Override
	public boolean hasPermission(Permission perm) {
		return hasPermission(perm.getName());
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
		return wrap.addAttachment(plugin, name, value);
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin) {
		return wrap.addAttachment(plugin);
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
		return wrap.addAttachment(plugin, name, value);
	}

	@Override
	public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
		return wrap.addAttachment(plugin, ticks);
	}

	@Override
	public void removeAttachment(PermissionAttachment attachment) {
		wrap.removeAttachment(attachment);
	}

	@Override
	public void recalculatePermissions() {
		wrap.recalculatePermissions();
	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return wrap.getEffectivePermissions();
	}

	@Override
	public boolean isOp() {
		return wrap.isOp();
	}

	@Override
	public void setOp(boolean value) {
		wrap.setOp(value);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof CommandSenderWrapper && wrap.equals(((CommandSenderWrapper) other).wrap);
	}
}
