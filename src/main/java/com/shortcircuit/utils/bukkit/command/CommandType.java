package com.shortcircuit.utils.bukkit.command;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * Designates the type of user which may run a command
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
public enum CommandType {
	/**
	 * Designates a command which may only be run by the server console
	 */
	CONSOLE("console", ConsoleCommandSender.class),
	/**
	 * Designates a command which may only be run by a player
	 */
	PLAYER("player", Player.class),
	/**
	 * Designates a command which may only be run by a command block
	 */
	BLOCK("block", BlockCommandSender.class),
	/**
	 * Designates a command which may be run by a player or a command block
	 */
	LOCATABLE("ingame", Player.class, BlockCommandSender.class, LocatableCommandSender.class),
	/**
	 * Designates a command which may be run by all users
	 */
	ANY("all", ConsoleCommandSender.class, Player.class, BlockCommandSender.class, LocatableCommandSender.class);

	private final String applicable_sender_name;
	private final Class<? extends CommandSender>[] applicable_senders;

	@SafeVarargs
	CommandType(String applicable_sender_name, Class<? extends CommandSender>... applicable_senders) {
		this.applicable_sender_name = applicable_sender_name;
		this.applicable_senders = applicable_senders;
	}

	public String getApplicableSenderName() {
		return applicable_sender_name;
	}

	public Class<? extends CommandSender>[] getApplicableSenders() {
		return applicable_senders.clone();
	}

	/**
	 * Check if a command sender is an allowed user
	 *
	 * @param sender The command sender to check
	 * @return <code>true</code> if the sender is allowed
	 */
	public boolean isSenderApplicable(CommandSender sender) {
		Class<? extends CommandSender> check = sender.getClass();
		for (Class<? extends CommandSender> applicable_sender : applicable_senders) {
			if (applicable_sender.isAssignableFrom(check)) {
				return true;
			}
		}
		return false;
	}
}
