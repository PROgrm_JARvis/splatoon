package com.shortcircuit.utils.bukkit;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 2/28/2017.
 */
public class PlayerJumpEvent extends PlayerMoveEvent {
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled = false;
	private final double jump_velocity;

	public PlayerJumpEvent(Player player, Location from, Location to, double jump_velocity) {
		super(player, from, to);
		this.jump_velocity = jump_velocity;
	}

	public double getJumpVelocity() {
		return jump_velocity;
	}
}
